import matplotlib as mpl
import matplotlib.mlab as mlab                                                                                                  
import matplotlib.pyplot as plt                                                                                                 
import matplotlib.axes as axe                                                                                                   
import matplotlib.collections as collections                                                                                    
import numpy as np                                                                                                              
import random                                                                                                                   
from pylab import *                                                                                                             
from matplotlib.pyplot import *

ANGLE = []
VELO = []
ANGLE.append("../rk_defangle.dat")
ANGLE.append("../data/eu_defangle_t225.dat")
ANGLE.append("../data/eu_defangle_t224.dat")
VELO.append("../rk_bif_avg_velo.dat")
VELO.append("../data/eu_defangle_velo_t255.dat")
VELO.append("../data/eu_defangle_velo_t224.dat")


FOUT = "../latex2/images/defangle.png"

xlabel = r'$h$'
ylabel = r'$\delta y$'
res = (1024/80,768/80)
pad1 = 1
pad2 = 0.1
tsize = 16
lsize = 24
max = 0.875
acangle = 58

######################################################################

angle = []
velo = []

for i in range(len(ANGLE)):
	angle.append(mlab.csv2rec(ANGLE[i], delimiter='\t', comments='#'))
for i in range(len(VELO)):
	velo.append(mlab.csv2rec(VELO[i], delimiter='\t', comments='#'))

r = []
psi = []
vx = []

for i in range(len(angle)):
	r.append(angle[i].r)
	psi.append(angle[i].x)
for i in range(len(velo)):
	vx.append(velo[i].x)



def mean_angle(param,angle):
	unique_param = unique(param)
	list_angles = [ [] for DUMMYVAR in range(len(unique_param)) ]
	ret_avg = []
	for i in range(len(unique_param)):
		for j in range(len(angle)):
			#print j, len(param), i, len(unique_param)
			if (param[j] == unique_param[i]):
				list_angles[i].append(angle[j])
				#print len(list_angles[i])
		ret_avg.append(abs(float(sum(list_angles[i])) /
			len(list_angles[i])))
		#print len(ret_avg)
	return ret_avg

color_list = [
		'black',
		'red',
		'green',
		]

marker_list = [
		'.',
		',',
		'o',
		'v',
		]

ls_list = [
		'-',
		'--',
		'-.',
		':',
		'.',
		',',
		'o',
		'v',
		]

print marker_list[0]

left, width = 0.1, 0.8
c = max/2
rect1 = [left, c+0.1, width, c]
rect2 = [left, 0.1, width, c]

rad_ac = acangle*(np.pi/180)

fig = plt.figure(figsize=res)

ax1 = fig.add_axes(rect1)
ax2 = fig.add_axes(rect2)

avg_angles = []
avg_velo = []
unique_r = []

for i in range(len(angle)):
	print "avg_angles[",i,"]"
	avg_angles.append(mean_angle(r[i],psi[i]))
	print "avg_velo[",i,"]"

	avg_velo.append(mean_angle(r[i],vx[i]))
	unique_r.append(unique(r[i]))

for i in range(len(angle)):
	ax1.plot(unique_r[i], avg_angles[i], marker=marker_list[i],
			ms=0, ls=ls_list[i], color=color_list[i])
for i in range(len(velo)):
	ax2.plot(unique_r[i], avg_velo[i], marker_list[i], ms=0,
			ls=ls_list[i], color=color_list[i])

xticks = np.arange(0,360+1,45)
yticks1 = np.arange(0,360+1,45)
yticks2 = np.arange(0,1.1,0.5)

ax1.set_xlim(r[0].min()-pad1,r[0].max()+pad1)
ax1.set_ylim(yticks1.min(),yticks1.max())
ax2.set_xlim(r[0].min()-pad2,r[0].max()+pad2)
ax2.set_ylim(0,vx[i].max()+pad2)
ax1.set_xticks([])
ax2.set_xticks(xticks)
ax1.set_yticks(yticks1)
ax2.set_yticks(yticks2)
ax1.axvline(acangle, color='black', ls=":")
#ax1.axvline(acangle-45, color='black', ls="-.")
#ax1.axvline(acangle+45, color='black', ls="-.")
ax1.axvline(acangle+180, color='black', ls=":")
#ax1.axvline(acangle+180-45, color='black', ls="-.")
#ax1.axvline(acangle+180+45, color='black', ls="-.")
ax2.axvline(acangle, color='black', ls=":")
ax2.axvline(acangle+180, color='black', ls=":")
#ax2.axvline(acangle+180-45, color='black', ls="-.")                                                                    
#ax2.axvline(acangle+180+45, color='black', ls="-.")
#ax2.axvline(acangle-45, color='black', ls="-.")                                                                        
#ax2.axvline(acangle+45, color='black', ls="-.")

ax1.set_ylabel(r'$\psi$', size=lsize)
ax2.set_ylabel(r'$v_x$', size=lsize)
ax2.set_xlabel(r'$\phi$', size=lsize)
ax2.set_xticklabels(ax2.get_xticks(), size=tsize)
ax1.set_yticklabels(ax1.get_yticks(), size=tsize)
ax2.set_yticklabels(ax2.get_yticks(), size=tsize)

plt.savefig(FOUT)

#plt.show()
