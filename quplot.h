#ifndef QUPLOT_H
#define QUPLOT_H

#include <QtGui/QMainWindow>
#include <QMap>
#include <QMessageBox>
#include <QVariant>
#include <QDebug>

class QAction;
class QMenu;
class QTextEdit;
class QCheckbox;
class QLabel;
class QLineEdit;
class QPushButton;

namespace Ui
{
    class QuPlot;
}

class QuPlot : public QMainWindow
{
    Q_OBJECT

public:
    QuPlot(QWidget *parent = 0);
    ~QuPlot();

    QMap<int, double> valuesMap;
    /* valuesMap containes the following values at index
    * Index         Value
    * 0         =   amount of independent functions to solve
    * 1         =   holds the function, 1: damped harmonic osz. \w ext. force,
    *               2: simple damped harmonic osz., 3: simple harmonic osz. \wo damping
    *           =   4: rotating dimer \w external force
    * 2         =   holds value of a
    * 3         =   holds value of A
    * 4         =   holds value of F
    * 5         =   holds value of l
    * 6         =   holds value of w
    * 7         =   holds values of t[0] = time
    * 8         =   holds values of x[0] = offset
    * 9         =   holds value of t[1] = unused
    * 10        =   holds value of x[1] = velocity
    * 11        =   mode, 0 = standard, 1 = converge, 2 = bifurcation
    * 12        =   is random ic?
    * 13        =   step size
    * 14        =   # of steps
    * 15        =   ic ? : from
    * 16        =   ic ? : to
    * 17        =   bifurcate from eta
    * 18        =   bifurcate to eta
    * 19        =   eta stepsize
    * 20        =   unused
    * 21        =   unused
    * 22        =   from t
    * 23        =   offy
    * 24        =   veloy
    * 25        =   eta1
    * 26        =   eta2
    * 27        =   phi0
    * 28        =   is random?
    * 29        =   is euler?
    * 30        =   searchmode
    * 31        =   threshold
    * 32        =   temperature
    * 33        =   unused
    * 34        =   unused
    * 35        =   no. of samples
    * 36        =   filter
    * 37        =   fromr2
    * 38        =   tor2
    * 39        =   stepr2
    * 40        =   theta
    * 41        =   psi
    * 42        =   U - amplitude of potential
    * 43        =   h step size
    */

    QString str1;
    QString str2;

    bool ruk;
    bool eul;

private slots:
    void setDimension();
    void about();
    void on_calcButton_clicked();
    void on_actionAbout_triggered();
    void on_actionQuit_triggered();
    void on_actionDimension_triggered();

private:
    Ui::QuPlot *ui;

    QDialog *dimDialog;

};

#endif // QUPLOT_H
