QuPlot calculates functions and bifurcations using 4th order Runge-Kutta. A
converge modus for testing is available as well.

WARNING: All data generated internally is saved in RAM. Since there is no
out-of-memory check yet, this program may crash your computer if you enter
unreasonable values. You have been warned!

All code is licensed under the common-sense license:

FOR TESTING PURPOSES ONLY. IT IS STRICTLY FORBIDDEN TO USE THE PROGRAM IF YOU DO
NOT KNOW WHAT YOU ARE DOING. ALTERING SOURCE CODE IS ALLOWED. FREE BEER IS ALWAYS
ACCEPTED AND ENCOURAGED.
