#ifndef QP3_H
#define QP3_H

// #define _USESTDVECTOR_
#define _CHECKBOUNDS_
//#define _QPENRERRORCLASS_ 1
//#define _TURNONFPES_ 1

// all the system #include's we'll ever need
#include <fstream>
#include <cmath>
#include <complex>
#include <iostream>
#include <iomanip>
#include <vector>
#include <limits>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>

using namespace std;
const double PI = M_PI;
// macro-like inline functions

template<class T>
inline T SQR(const T a) {return a*a;}

template<class T>
inline T ABS(const T a) {return a < 0 ? (a+2*PI) : a;}

template<class T>
inline T SIGN(const T &a, const T &b)
        {return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);}

inline float SIGN(const float &a, const double &b)
        {return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);}

inline float SIGN(const double &a, const float &b)
        {return (float)(b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a));}

// exception handling

#ifndef _USEQPERRORCLASS_
#define throw(message) \
{printf("ERROR: %s\n	in file %s at line %d\n",  message,__FILE__,__LINE__); throw(1);}
#else
struct QPerror {
	char *message;
	char *file;
	int line;
	QPerror(char *m, char *f, int l) : message(m), file(f), line(l) {}
};
#define throw(message) throw(QPerror(message,__FILE__,__LINE__));
void QPcatch(QPerror err) {
	printf("ERROR: %s\n	in file %s at line %d\n",
			err.message, err.file, err.line);
	exit(1);
}
#endif

#ifdef _USESTDVECTOR_
#define QPvector vector
#else

template<class T>
class QPvector {
private:
    int nn; // size of array. upper index is nn-1
    T *v;
public:
	QPvector();
	explicit QPvector(int n);		// Zero-based array;
	QPvector(int n, const T &a);	//initialize to constant value
	QPvector(int n, const T *a);	// Initialize to array
	QPvector(const QPvector &rhs);  // Copy constructor
	QPvector & operator=(const QPvector &rhs);	//assignment
    typedef T value_type; // make T available externally
    inline T & operator[](const int i); //i'th element
    inline const T & operator[](const int i) const;
    inline int size() const;
    void resize(int newn); // resize (contents not preserved)
    void assign(int newn, const T &a); // resize and assign a constant value
    ~QPvector();
};

// QPvector definitions

template <class T>
QPvector<T>::QPvector() : nn(0), v(NULL) {}

template <class T>
QPvector<T>::QPvector(int n) : nn(n), v(n>0 ? new T[n] : NULL) {}

template <class T>
QPvector<T>::QPvector(int n, const T& a) : nn(n), v(n>0 ? new T[n] : NULL)
{
    for(int i=0; i<n; i++) v[i] = a;
}

template <class T>
QPvector<T>::QPvector(int n, const T *a) : nn(n), v(n>0 ? new T[n] : NULL)
{
    for(int i=0; i<n; i++) v[i] = *a++;
}

template <class T>
QPvector<T>::QPvector(const QPvector<T> &rhs) : nn(rhs.nn), v(nn>0 ? new T[nn] : NULL)
{
    for (int i=0; i<nn; i++) v[i] = rhs[i];
}

template <class T>
QPvector<T> & QPvector<T>::operator=(const QPvector<T> &rhs)
// postcondition: normal assignment via copying has been performed;
//	if vector and rhs were different sizes, vector
//	has been resized to match the size of rhs
{
	if (this != &rhs)
	{
		if (nn != rhs.nn) {
		if (v != NULL) delete [] (v);
		nn=rhs.nn;
		v= nn>0 ? new T[nn] : NULL;
	}
		for (int i=0; i<nn; i++)
			v[i]=rhs[i];
	}
	return *this;
}

template <class T>
inline T & QPvector<T>::operator[](const int i)	//subscripting
{
#ifdef _CHECKBOUNDS_
if (i<0 || i>=nn) {
        throw("QPvector subscript out of bounds");
}
#endif
    return v[i];
}

template <class T>
inline const T & QPvector<T>::operator[](const int i) const	//subscripting
{
#ifdef _CHECKBOUNDS_
if (i<0 || i >=nn) {
    throw("QPvector subscript 2 out of bounds");
}
#endif
    return v[i];
}

template <class T>
inline int QPvector<T>::size() const
{
    return nn;
}

template <class T>
void QPvector<T>::resize(int newn)
{
    if (newn != nn) {
        if (v != NULL) delete[] (v);
        nn = newn;
        v = nn > 0 ? new T[nn] : NULL;
    }
}

template <class T>
void QPvector<T>::assign(int newn, const T& a)
{
    if (newn != nn) {
        if (v != NULL) delete[] (v);
        nn = newn;
        v = nn > 0 ? new T[nn] : NULL;
    }
    for (int i=0;i<nn;i++) v[i] = a;
}

template <class T>
QPvector<T>::~QPvector()
{
    if (v != NULL) delete[] (v);
}

// end of QPvector definitions

#endif //ifdef _USESTDVECTOR_

// basic type names

typedef int Int; // 32 bit integer
typedef unsigned int Uint;

typedef long long int Llong; // 64 bit integer
typedef unsigned long long int Ullong;

typedef double Doub; // default floating type
typedef long double Ldoub;

typedef bool Bool;

// vector types

typedef const QPvector<Int> VecInt_I;
typedef QPvector<Int> VecInt, VecInt_O, VecInt_IO;

typedef const QPvector<Doub> VecDoub_I;
typedef QPvector<Doub> VecDoub, VecDoub_O, VecDoub_IO;

#endif // QP3_H
