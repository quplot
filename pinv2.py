#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import matplotlib as mpl
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.colors as col

FOUT = '../latex2/images/dimer_sympartner.png'
FCOF = '../rk_out.dat'
FP1 = '../rk_point1.dat'
FP2 = '../rk_point2.dat'
FVEL = "../rk_velo.dat"

L = 2*np.pi
w = 0.1
tau = L/w
evry = 1 # plot periodicity, don't set < 10 (bug)
offx = -98 # offset of (x,y) in L
offy = 94
tfrom = 80 # from t
tto = 100 # to t
acangle = 58 # angle of ac drive
dcangle = 0 # angle of dc bias
sperx = 4 # spacial periodicity
spery = 3
pad = 0.01 # small padding so the ticks get drawn correctly...
msize = 40
res = (1440/80,900/80) # default dpi is 80
cmap = cm.gray_r # colormap
shader = False # applies ultra cool shadows to the map!
show_plot = False
xlabel = r'$x$'
ylabel = r'$y$'
cblabel = r'$\cos(x)\cos(y)+\cos(x)+\cos(y)$'
fs_ticks = 16
fs_labels = 24
cof_only = False
has_dc = False
test = False
show_cb = False
show_legend = False
show_annotations = False
thresh = 1
title = 'F = 0.1, alpha = 83, l = 3.0, a = 1.5, eta1 = 1.0, eta2 = 1.5, theta = 0'

################################################################################

print "reading data..."

data_cof= mlab.csv2rec(FCOF, delimiter='\t', comments='#')
data_p1 = mlab.csv2rec(FP1, delimiter='\t', comments='#')
data_p2 = mlab.csv2rec(FP2, delimiter='\t', comments='#')
data_v = mlab.csv2rec(FVEL, delimiter='\t', comments='#')

t = data_cof.r
cofx = data_cof.x
cofy = data_cof.y
p1x = data_p1.x
p1y = data_p1.y
p2x = data_p2.x
p2y = data_p2.y
pxdiff = data_p2.x - data_p1.x
pydiff = data_p2.y - data_p1.y
vx = data_v.x
vy = data_v.y

print "done."

tstep = t[10] - t[9]

fromt = tfrom/tstep
tot = tto/tstep

tcx = [0.59,0.65,0.65,0.57,0.52,0.39]
tcy = [0.4,0.6,0.83,0.6,0.51,0.4]
tl = ["1","2","3","4","5","6"]

every = evry*tstep
trunc_cofx = cofx[fromt*tau:tot*tau:evry].tolist()
trunc_cofy = cofy[fromt*tau:tot*tau:evry].tolist()
trunc_p1x = p1x[fromt*tau:tot*tau:evry].tolist()
trunc_p2x = p2x[fromt*tau:tot*tau:evry].tolist()
trunc_p2y = p2y[fromt*tau:tot*tau:evry].tolist()
trunc_p1y = p1y[fromt*tau:tot*tau:evry].tolist()
trunc_pxdiff = pxdiff[fromt*tau:tot*tau:evry].tolist()
trunc_pydiff = pydiff[fromt*tau:tot*tau:evry].tolist()
trunc_t = t[fromt*tau:tot*tau:evry].tolist()
trunc_vx = vx[fromt*tau:tot*tau:evry].tolist()
trunc_vy = vy[fromt*tau:tot*tau:evry].tolist()

def uniquify(seq, idfun=None):
	# order preserving 
	if idfun is None: 
		def idfun(x): return x 
	seen = {} 
	result = [] 
	for item in seq: 
		marker = idfun(item) 
		# in old Python versions: 
		# # if seen.has_key(marker) 
		# # but in new ones: 
		if marker in seen: continue 
		seen[marker] = 1 
		result.append(item) 
	return result

trunc_p1x_eff = []
trunc_p2x_eff = []
trunc_p1y_eff = []
trunc_p2y_eff = []
trunc_pxdiff_eff = []
trunc_pydiff_eff = []
trunc_vx_eff = []
trunc_vy_eff = []

for i in range(len(trunc_cofx)):
	assert len(trunc_cofx) == len(trunc_p1x)
	#print i, len(trunc_p1x_eff), len(trunc_cofx)
	vel = np.sqrt(pow(trunc_vx[i],2)+pow(trunc_vy[i],2))
	p1 = abs(np.sqrt(pow(trunc_p1x[i],2)+pow(trunc_p1y[i],2))-np.sqrt(pow(trunc_p1x[i-1],2)+pow(trunc_p1y[i-1],2)))
	p2 = abs(np.sqrt(pow(trunc_p2x[i],2)+pow(trunc_p2y[i],2))-np.sqrt(pow(trunc_p2x[i-1],2)+pow(trunc_p2y[i-1],2)))
	if (i != 0):
		if (p1 < thresh) and (p2 < thresh):
			trunc_cofx[i] = trunc_cofx[i-1]
			trunc_cofy[i] = trunc_cofy[i-1]
			trunc_p1x[i] = trunc_p1x[i-1]
			trunc_p2x[i] = trunc_p2x[i-1]
			trunc_p1y[i] = trunc_p1y[i-1]
			trunc_p2y[i] = trunc_p2y[i-1]
			trunc_pxdiff[i] = trunc_pxdiff[i-1]
			trunc_pydiff[i] = trunc_pydiff[i-1]
			trunc_vx[i] = trunc_vx[i-1]
			trunc_vy[i] = trunc_vy[i-1]
	else:
		print "velocity:",vel, "dp1:", p1, "dp2:", p2

set_p1x = trunc_p1x#uniquify(trunc_p1x)
set_p1y = trunc_p1y#uniquify(trunc_p1y)
set_p2x = trunc_p2x#uniquify(trunc_p2x)
set_p2y = trunc_p2y#uniquify(trunc_p2y)
set_pxdiff = trunc_pxdiff#uniquify(trunc_pxdiff)
set_pydiff = trunc_pydiff#uniquify(trunc_pydiff)
set_vx = trunc_vx#uniquify(trunc_vx)
set_vy = trunc_vy#uniquify(trunc_vy)
set_cofx = trunc_cofx
set_cofy = trunc_cofy


print len(set_cofx), len(set_cofy)

xmax = np.ceil(max(trunc_cofx)/L)
xmin = np.ceil(min(trunc_cofx)/L)
ymax = np.ceil(max(trunc_cofy)/L)
ymin = np.ceil(min(trunc_cofy)/L)

xdiff = np.ceil(abs(max(trunc_cofx)-min(trunc_cofx))/L)
ydiff = np.ceil(abs(max(trunc_cofy)-min(trunc_cofy))/L)

xcorr = xdiff-sperx
ycorr = ydiff-spery

#print xdiff, ydiff
#print xdiff-(xdiff-sper)

if xmax > 0:
	limx_min = (xmin+2)*L
	limx_max = (xmin+2+sperx)*L
if xmax < 0:
	limx_min = (xmax-2-sperx)*L
	limx_max = (xmax-2)*L
if ymax > 0:
	limy_min = (ymin-2)*L
	limy_max = (ymin-2+spery)*L
if ymax < 0:
	limy_min = (ymax-spery)*L
	limy_max = (ymax)*L

#print (limx_max-limx_min)/L

print "x:","(",limx_min,",",limx_max,")"
print "y:","(",limy_min,",",limy_max,")"

xticks = np.arange(limx_min,limx_max+pad,L)
yticks = np.arange(limy_min,limy_max+pad,L)
xlim = (limx_min-pad,limx_max+pad)
ylim = (limy_min-pad,limy_max+pad)
tlabel = ("-2L","-L","0","L","2L")

print "generating basemap..."
delta = 0.01
x = np.arange(limx_min,limx_max, delta)
y = np.arange(limy_min,limy_max, delta)
X,Y = np.meshgrid(x, y)
Z = np.cos(X)*np.cos(Y)+np.cos(X)+np.cos(Y)
print "done."

if test is True:
	raise

fig = plt.figure() #(figsize=res)

ax = fig.add_subplot(111)

if (shader is True):
    print "applying lightsource..."
    ls = col.LightSource(azdeg=0,altdeg=90)
    rgb = ls.shade(Z,cmap)
    print "done."
    print "plotting basemap..."
    pot = plt.imshow(rgb, extent=[limx_min,limx_max,limy_min,limy_max])
    print "done."
else:
    print "plotting basemap..."
    pot = plt.pcolormesh(X,Y,Z, cmap=cmap)
    print "done."

if (show_cb is True):
	cb = plt.colorbar(pot)
	cb.set_label(cblabel)

print "plotting dimer onto the basemap..."
if (cof_only is False):
	cof = ax.plot(trunc_cofx, trunc_cofy, c='green', marker='.',
			lw=1, label='cof', alpha=0.5)
	x1 = ax.scatter(set_p1x, set_p1y, c='black', marker='o', lw=0, s=msize, label='x1')
	x2 = ax.scatter(set_p2x, set_p2y, c='grey', marker='o', lw=0, s=msize, label='x2') 

	k=0
	for i in range(len(set_p1x)):
		if trunc_t[i] >= k*every:
			#print "plotting because",t[i],"=",k,"*",every,"(",k*every,")"
			bond = ax.arrow(set_p1x[i], set_p1y[i],
					set_pxdiff[i],
					set_pydiff[i], color='black',
					lw=1, label='bond',
					alpha=0.5)
			velo = ax.arrow(set_cofx[i], set_cofy[i],
					2*set_vx[i], 2*set_vy[i],
					color='red', lw=1,
					head_width=0.15, shape='full',
					label='velo')
			k = k+1
		elif trunc_t[i] < k*every:
			#print "not plotting because",t[i],"!=",k,"*",every,"(",k*every,")"
			continue
elif (cof_only is True):
	cof = ax.scatter(trunc_cofx, trunc_cofy, c='red', marker='o',
			lw=0, s=msize/4, label='cof')
else:
	print "fatal error!"


print "done."

ax.arrow(limx_min+L/2,limy_min+L/2, 2*np.cos(acangle*np.pi/180),
		2*np.sin(acangle*np.pi/180), lw=2.5, color='blue',
		head_width=0.15, shape='full')
ax.arrow(limx_min+L/2,limy_min+L/2, -2*np.cos(acangle*np.pi/180),
		-2*np.sin(acangle*np.pi/180), lw=2.5, color='blue',
		head_width=0.15, shape='full')
if (has_dc is True):
	ax.arrow(limx_min+L/2,limy_min+L/2, 2*np.cos(dcangle*np.pi/180),
			2*np.sin(dcangle*np.pi/180), lw=2.5, color='red',
			head_width=0.15, shape='full')

if (show_annotations is True):
	for i in range(len(tcx)):
		fig.text(tcx[i],tcy[i],tl[i],bbox=dict(boxstyle="round",
			fc=(1.0, 0.7, 0.7), ec="none"))


ax.set_xlabel(xlabel, size=fs_labels)
ax.set_ylabel(ylabel, size=fs_labels)
ax.set_xticks(xticks)
ax.set_yticks(yticks)
ax.set_xlim(limx_min,limx_max)
ax.set_ylim(limy_min,limy_max)
ax.set_xticklabels(tlabel, size=fs_ticks)
ax.set_yticklabels(tlabel, size=fs_ticks)

if (show_legend is True):
	legend = fig.legend((x1, x2, bond), (r'$\vec{r}_1$',
		r'$\vec{r}_2$', r'$|\vec{r}_1-\vec{r}_2|$'), shadow=True,
		fancybox=True)
"""
elif (cof_only is True):
	legend = fig.legend((cof),(r'$\vec{r}_s$'), shadow=True, fancybox=True)
else:
	print "fatal error!"

#legend.get_frame().set_alpha(0.75)
"""

#plt.title(title)

print "saving to file.."
plt.savefig(FOUT)
print "done."

if (show_plot is True):
	print "showing plot..."
	plt.show()
	print "done."


