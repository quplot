#ifndef DIMDIALOG_H
#define DIMDIALOG_H

#include <ctime>

#include <QtGui/QDialog>
#include <QLabel>
#include <QComboBox>
#include <QSpinBox>
#include <QStringList>
#include <QCheckBox>
#include "quplot.h"
#include "ran.h"

namespace Ui {
    class DimDialog;
}

class DimDialog : public QDialog {
    Q_OBJECT
    Q_DISABLE_COPY(DimDialog)

public:
    explicit DimDialog(QuPlot *parent = 0);
    virtual ~DimDialog();

        QuPlot *d;

protected:
   virtual void changeEvent(QEvent *e);
    void initDialog();

private slots:
    void on_pushButton_clicked();
    void on_convergeCheck_stateChanged();
    void on_bifurcateCheck_stateChanged();
    void on_dualbifCheck_stateChanged();
    void on_randomCheck_stateChanged();
    void on_searchCheck_stateChanged();
    void on_comboBox_currentIndexChanged();
//    void on_spinBox_valueChanged(int value);
//    void on_f0Line_editingFinished();
//    void on_f1Line_editingFinished();

private:
    Ui::DimDialog *m_ui;

    double randpi(Ran &r);
    double pi;

    QComboBox *c;
    QDoubleSpinBox *from;
    QDoubleSpinBox *to;
    QDoubleSpinBox *stepsize;
    QDoubleSpinBox *steps;
    QDoubleSpinBox *velox;
    QDoubleSpinBox *veloy;

    QStringList list;
    QCheckBox *cmBox;
    QCheckBox *bfBox;

    bool randcheck;
    bool cmcheck;
    bool bfcheck;
    bool bf2check;
    bool searchcheck;

};

#endif // DIMDIALOG_H
