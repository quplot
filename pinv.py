#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import matplotlib as mpl
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.colors as col

FOUT = '../dimer_in_potential.png'
FCOF = '../eu_out.dat'
FP1 = '../eu_point1.dat'
FP2 = '../eu_point2.dat'

L = 2*np.pi
w = 0.1
tau = L/w
evry = 1000 # plot periodicity, don't set < 10 (bug)
offx = -98 # offset of (x,y) in L
offy = 94
tfrom = 80 # from t
tto = 100 # to t
acangle = 58 # angle of ac drive
dcangle = 0 # angle of dc bias
sper = 4 # spacial periodicity
pad = 0.01 # small padding so the ticks get drawn correctly...
msize = 20
res = (1440/80,900/80) # default dpi is 80
cmap = cm.gray_r # colormap
shader = False # applies ultra cool shadows to the map!
show_plot = False
xlabel = r'$x$'
ylabel = r'$y$'
cblabel = r'$\cos(x)\cos(y)+\cos(x)+\cos(y)$'
fs_ticks = 16
fs_labels = 24
cof_only = True
has_dc = False
title = 'F = 0.1, alpha = 83, l = 3.0, a = 1.5, eta1 = 1.0, eta2 = 1.5, theta = 0'

################################################################################

print "reading data..."

data_cof= mlab.csv2rec(FCOF, delimiter='\t', comments='#')
data_p1 = mlab.csv2rec(FP1, delimiter='\t', comments='#')
data_p2 = mlab.csv2rec(FP2, delimiter='\t', comments='#')

t = data_cof.r
cofx = data_cof.x
cofy = data_cof.y
p1x = data_p1.x
p1y = data_p1.y
p2x = data_p2.x
p2y = data_p2.y
pxdiff = data_p2.x - data_p1.x
pydiff = data_p2.y - data_p1.y

print "done."

tstep = t[10] - t[9]

fromt = tfrom/tstep
tot = tto/tstep

every = evry*tstep
trunc_cofx = cofx[fromt*tau:tot*tau:evry]
trunc_cofy = cofy[fromt*tau:tot*tau:evry]
trunc_p1x = p1x[fromt*tau:tot*tau:evry]
trunc_p2x = p2x[fromt*tau:tot*tau:evry]
trunc_p2y = p2y[fromt*tau:tot*tau:evry]
trunc_p1y = p1y[fromt*tau:tot*tau:evry]
trunc_pxdiff = pxdiff[fromt*tau:tot*tau:evry]
trunc_pydiff = pydiff[fromt*tau:tot*tau:evry]
trunc_t = t[fromt*tau:tot*tau:evry]

xmax = np.ceil(max(trunc_cofx)/L)
xmin = np.ceil(min(trunc_cofx)/L)
ymax = np.ceil(max(trunc_cofy)/L)
ymin = np.ceil(min(trunc_cofy)/L)

xdiff = np.ceil(abs(max(trunc_cofx)-min(trunc_cofx))/L)
ydiff = np.ceil(abs(max(trunc_cofy)-min(trunc_cofy))/L)

xcorr = xdiff-sper
ycorr = ydiff-sper

print xdiff, ydiff
print xdiff-(xdiff-sper)

if xmax > 0:
	limx_min = (xmin+2)*L
	limx_max = (xmin+2+sper)*L
if xmax < 0:
	limx_min = (xmax-1-sper)*L
	limx_max = (xmax-1)*L
if ymax > 0:
	limy_min = (ymin-2)*L
	limy_max = (ymin-2+sper)*L
if ymax < 0:
	limy_min = (ymax-sper)*L
	limy_max = (ymax)*L

print (limx_max-limx_min)/L

print "x:","(",limx_min,",",limx_max,")"
print "y:","(",limy_min,",",limy_max,")"

xticks = np.arange(limx_min,limx_max+pad,L)
yticks = np.arange(limy_min,limy_max+pad,L)
xlim = (limx_min-pad,limx_max+pad)
ylim = (limy_min-pad,limy_max+pad)
tlabel = ("-2L","-L","0","L","2L")

print "generating basemap..."
delta = 0.01
x = np.arange(limx_min,limx_max, delta)
y = np.arange(limy_min,limy_max, delta)
X,Y = np.meshgrid(x, y)
Z = np.cos(X)*np.cos(Y)+np.cos(X)+np.cos(Y)
print "done."

fig = plt.figure(figsize=res)

ax = fig.add_subplot(111)

if (shader is True):
    print "applying lightsource..."
    ls = col.LightSource(azdeg=0,altdeg=90)
    rgb = ls.shade(Z,cmap)
    print "done."
    print "plotting basemap..."
    pot = plt.imshow(rgb, extent=[limx_min,limx_max,limy_min,limy_max])
    print "done."
else:
    print "plotting basemap..."
    pot = plt.pcolormesh(X,Y,Z, cmap=cmap)
    print "done."
    
cb = plt.colorbar(pot)
cb.set_label(cblabel)

print "plotting dimer onto the basemap..."
if (cof_only is False):
	cof = ax.scatter(trunc_cofx, trunc_cofy, c='yellow', marker='o', lw=0, s=msize/4, label='cof')
	x1 = ax.scatter(trunc_p1x, trunc_p1y, c='blue', marker='o', lw=0, s=msize, label='x1')
	x2 = ax.scatter(trunc_p2x, trunc_p2y, c='purple', marker='o', lw=0, s=msize, label='x2') 

	k=0
	for i in range(len(trunc_t)):
		if trunc_t[i] >= k*every:
			#print "plotting because",t[i],"=",k,"*",every,"(",k*every,")"
			bond = ax.arrow(trunc_p1x[i], trunc_p1y[i],
					trunc_pxdiff[i],
					trunc_pydiff[i], color='r',
					lw=0.1, label='bond',
					alpha=0.5)
			k = k+1
		elif trunc_t[i] < k*every:
			#print "not plotting because",t[i],"!=",k,"*",every,"(",k*every,")"
			continue
elif (cof_only is True):
	cof = ax.scatter(trunc_cofx, trunc_cofy, c='red', marker='o',
			lw=0, s=msize/4, label='cof')
else:
	print "fatal error!"


print "done."

ax.arrow(limx_min,limy_min, 10*np.cos(acangle*np.pi/180),
		10*np.sin(acangle*np.pi/180), lw=2.5, color='blue',
		head_width=0.15, shape='full')

if (has_dc is True):
	ax.arrow(limx_min,limy_min, 10*np.cos(dcangle*np.pi/180),
			10*np.sin(dcangle*np.pi/180), lw=2.5, color='red',
			head_width=0.15, shape='full')

ax.set_xlabel(xlabel, size=fs_labels)
ax.set_ylabel(ylabel, size=fs_labels)
ax.set_xticks(xticks)
ax.set_yticks(yticks)
ax.set_xlim(limx_min,limx_max)
ax.set_ylim(limy_min,limy_max)
ax.set_xticklabels(tlabel, size=fs_ticks)
ax.set_yticklabels(tlabel, size=fs_ticks)

if (cof_only is False):
	legend = fig.legend((cof, x1, x2, bond), (r'$\vec{r}_s$', r'$\vec{r}_1$',
		r'$\vec{r}_2$', r'$|\vec{r}_1-\vec{r}_2|$'), shadow=True,
		fancybox=True)
"""
elif (cof_only is True):
	legend = fig.legend((cof),(r'$\vec{r}_s$'), shadow=True, fancybox=True)
else:
	print "fatal error!"

#legend.get_frame().set_alpha(0.75)
"""

#plt.title(title)

print "saving to file.."
plt.savefig(FOUT)
print "done."

if (show_plot is True):
	print "showing plot..."
	plt.show()
	print "done."

