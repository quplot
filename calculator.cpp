//=============================================================================
//
// CLASS Calculator
// contains all the magic math
//
//=============================================================================


//== INCLUDES =================================================================

#include "calculator.h"


//== IMPLEMENTATION ===========================================================


Calculator::Calculator(QuPlot *parent) :
        QDialog(parent)
{
        c = parent;
	derive_init();

}

Calculator::~Calculator()
{
}


//-----------------------------------------------------------------------------
void Calculator::derive_init()
{
    file_rk_bif = "../rk_bifurcate.dat";
    file_rk_avg = "../rk_bif_avg_velo.dat";
    file_rk_out = "../rk_out.dat";
    file_rk_pt1 = "../rk_point1.dat";
    file_rk_pt2 = "../rk_point2.dat";
    file_rk_biv = "../rk_bif_velo.dat";
    file_rk_vel = "../rk_velo.dat";

    file_eu_bif = "../eu_bifurcate.dat";
    file_eu_avg = "../eu_bif_avg_velo.dat";
    file_eu_out = "../eu_out.dat";
    file_eu_pt1 = "../eu_point1.dat";
    file_eu_pt2 = "../eu_point2.dat";
    file_eu_biv = "../eu_bif_velo.dat";
    file_eu_vel = "../eu_velo.dat";

    file_rk_conv = "../rk_conv.dat";
    file_eu_conv = "../eu_conv.dat";

    file_rk_variance = "../rk_variance.dat";
    file_rk_distance = "../rk_msd.dat";
    file_rk_defangle = "../rk_defangle.dat";
    file_eu_variance = "../eu_variance.dat";
    file_eu_distance = "../eu_msd.dat";
    file_eu_defangle = "../eu_defangle.dat";

    file_rk_dualbif = "../rk_dualbif.dat";
    file_eu_dualbif = "../eu_dualbif.dat";

    v = c->valuesMap.value(1);

}

void Calculator::derivs(const Doub x, VecDoub_I &y, VecDoub_O &dydx,
                        const Doub params[])
{
    //params[0] = a;
    //params[1] = A;
    //params[2] = F;
    //params[3] = l;
    //params[4] = w;
    //params[5] = eta1;
    //params[6] = eta2;
    //params[7] = l1;
    //params[8] = l2;
    //params[9] = T;
    //params[10] = theta;
    //params[11] = e0;
    //params[12] = e1;
    //params[13] = e2;
    //params[14] = e3;
    //params[15] = fx;
    //params[16] = fy;
    //params[17] = psi;
    //params[18] = U;
    //static Doub G, xi, xi1, xi2, c, s, st, ax, ay, e0, e1, e2, e3, eta, l1, l2;
    static Doub xi, G, s, c, st, ax, ay, cp, sp, m1, m2, p1, p2;
    static Doub x1, y1, x2, y2, F1x, F1y, F2x, F2y, stx, sty;
    //for (Int i = 0;i<10;i++)
     //   qDebug() << "params["<<i<<"] :" << params[i];

    switch (v) {
    case 1:
        G = sqrt(2.0*params[5]*params[9])*xi;
        dydx[0] = y[1];
        dydx[1] = -params[5]*y[1]-sin(y[0])+params[1]*sin(params[4]*x)+params[2]+G;
        break;
    case 2:
        dydx[0] = y[1];
        dydx[1] = -params[5]*y[1]-y[0];
        break;
    case 3:
        dydx[0] = y[1];
        dydx[1] = -y[0];
        break;
    case 4:

        /*
        xix1 = gauss.dev();
        xix2 = gauss.dev();
        xiy1 = gauss.dev();
        xiy2 = gauss.dev();
        */
        /*
        eta = params[5]+params[6];
        e0 = 1.0/eta;
        e1 = 1.0/(params[5]*params[3]);
        e2 = 1.0/(params[6]*params[3]);
        e3 = 1.0/params[3]*((1.0/params[5])-(1.0/params[6]));

        l1 = params[3]*e0*params[5];
        l2 = params[3]*e0*params[6];

        */

       //qDebug() << eta << "=" << params[5] << "+" << params[6];
       //qDebug() << e0 << "=" << "1.0/"<< eta;
       //qDebug() << e1 << "=" << "1.0/"<<params[5]<<"*"<<params[3];
       //qDebug() << e2 << "=" << "1.0/"<<params[6]<<"*"<<params[3];
       //qDebug() << e3 << "=" << "1.0/"<<params[5]<<"-"<<"1.0/"<<params[6];

        //G = sqrt(2.0*eta*params[9]);
        G = 0;

        c = cos(y[2]);
        s = sin(y[2]);

        x1 = y[0] + params[7]*c;
        y1 = y[1] + params[7]*s;
        x2 = y[0] - params[8]*c;
        y2 = y[1] - params[8]*s;

        st = sin(params[4]*x);

        //qDebug() << st << "=" << "sin("<<params[4]<<"*"<<x<<")";

        ax = cos(params[1])*params[0]*st;
        ay = sin(params[1])*params[0]*st;

        //fx = cos(params[10])*params[2];
        //fy = sin(params[10])*params[2];

        stx = ax+params[15];
        sty = ay+params[16];

        //qDebug() << "ax =" << ax << "=" << params[0] << "*" << "cos("<<params[1]<<")";
        //qDebug() << "ay =" << ay << "=" << params[0] << "*" << "sin("<<params[1]<<")";
        //qDebug() << "fx =" << fx << "=" << "cos("<<params[10]<<")*"<<params[2];
        //qDebug() << "fy =" << fy << "=" << "sin("<<params[10]<<")*"<<params[2];
        //qDebug() << "stx =" << stx <<"="<< ax << "*" << st << "+"<< fx;
        //qDebug() << "sty =" << sty <<"="<< ay << "*" << st << "+" << fy;



        //qDebug() << y[0] + params[7]*c << "=" << y[0] << "+" << params[7] << "*" << c;
        //qDebug() << y[1] + params[7]*c << "=" << y[1] << "+" << params[7] << "*" << s;
        //qDebug() << y[0] - params[8]*c << "=" << y[0] << "-" << params[8] << "*" << c;
        //qDebug() << y[1] - params[8]*c << "=" << y[1] << "-" << params[8] << "*" << s;

        cp = cos(params[17]);
        sp = sin(params[17]);

        m1 = cp*x1-sp*y1;
        p1 = sp*x1+cp*y1;
        m2 = cp*x2-sp*y2;
        p2 = sp*x2+cp*y2;

        F1x = params[18]*(sin(m1)*cos(p1)*cp+cos(m1)*sin(p1)*sp+sin(m1)*cp+sin(p1)*sp);//+1.6*sin(x1)+1.0*cos(y1);
        F1y = params[18]*(-sin(m1)*cos(p1)*sp+cos(m1)*sin(p1)*cp-sin(m1)*sp+sin(p1)*cp);//+1.6*cos(x1)+1.0*sin(y1);
        F2x = params[18]*(sin(m2)*cos(p2)*cp+cos(m2)*sin(p2)*sp+sin(m2)*cp+sin(p2)*sp);//+1.6*sin(x2)+1.0*cos(y2);
        F2y = params[18]*(-sin(m2)*cos(p2)*sp+cos(m2)*sin(p2)*cp-sin(m2)*sp+sin(p2)*cp);//+1.6*cos(x2)+1.0*sin(y2);

        dydx[0] = (F1x+F2x+2.0*stx/*+sqrt(2*params[9])*(xix1+xix2)*/)*params[11];
        dydx[1] = (F1y+F2y+2.0*sty/*+sqrt(2*params[9])*(xiy1+xiy2)*/)*params[11];
        dydx[2] = params[12]*(c*F1y-s*F1x)-params[13]*(c*F2y-s*F2x)+params[14]*(c*sty-s*stx)/*+sqrt(2*params[9])*(params[12]*(-s*xix1+c*xiy1)-params[13]*(-s*xix2+c*xiy2))*/;

        break;
    }
}

void Calculator::derivw(const Doub x, VecDoub_I &y, VecDoub_O &dW,
       const Doub params[], Normaldev &gauss)
{
    //params[0] = a;
    //params[1] = A;
    //params[2] = F;
    //params[3] = l;
    //params[4] = w;
    //params[5] = eta1;
    //params[6] = eta2;
    //params[7] = l1;
    //params[8] = l2;
    //params[9] = T;
    //params[10] = theta;
    //params[11] = e0;
    //params[12] = e1;
    //params[13] = e2;
    //params[14] = e3;
    //params[15] = fx;
    //params[16] = fy;
    //params[17] = psi;
    //params[18] = U;

    Doub xix1, xix2, xiy1, xiy2;
    Doub c, s, T, l, eta1, eta2;

    xix1 = gauss.dev();
    xix2 = gauss.dev();
    xiy1 = gauss.dev();
    xiy2 = gauss.dev();

    T = params[9];
    l = params[7] + params[8];
    eta1 = params[5];
    eta2 = params[6];

    c = cos(y[2]);
    s = sin(y[2]);

    switch (v) {
        case 4:
        dW[0] = sqrt(2*T)*(sqrt(eta1)*xix1+sqrt(eta2)*xix2);
        dW[1] = sqrt(2*T)*(sqrt(eta1)*xiy1+sqrt(eta2)*xiy2);
        dW[2] = (sqrt(2*T)/l)*((1/sqrt(eta1))*(-s*xix1+c*xiy1)-(1/sqrt(eta2))*(-s*xix2+c*xiy2));
        break;
    }
}
//-----------------------------------------------------------------------------


void Calculator::rk4(VecDoub_I &y, VecDoub_I &dydx, const Doub x, const Doub h,
                     VecDoub_O &yout, const Doub params[])
{
    Int n = y.size();
    VecDoub dym(n),dyt(n),yt(n);
    Doub hh = h*0.5;
    Doub h6 = h/6.0;
    Doub xh = x+hh;
    for (Int i=0;i<n;i++) yt[i] = y[i]+hh*dydx[i];
    derivs(xh,yt,dyt,params);
    for (Int i=0;i<n;i++) yt[i] = y[i]+hh*dyt[i];
    derivs(xh,yt,dym,params);
    for (Int i=0;i<n;i++) {
        yt[i] = y[i]+h*dym[i];
        dym[i] += dyt[i];
    }
    derivs(x+h,yt,dyt,params);
    for (Int i=0;i<n;i++) {
        yout[i] = y[i]+h6*(dydx[i]+dyt[i]+2.0*dym[i]);
    }
}


void Calculator::euler(VecDoub_I &y, VecDoub_I &dydx, VecDoub_I &dW, const Doub x,
                       const Doub h, VecDoub_O &yout)
{
    Int n = y.size();
    for (Int i=0;i<n;i++) {
        yout[i] = y[i]+h*dydx[i]+sqrt(h)*dW[i];
    }
}
//-----------------------------------------------------------------------------


/*
 * returns a random number between 0 and 2*pi
 */
double Calculator::randpi(Ran &r)
{
    Doub s = 2.0*M_PI*r.doub();
    return s;
}

//-----------------------------------------------------------------------------

/*
 * Writes the of two maps into a tab-separated file
 * The second map *must* be specified.
 */
void Calculator::write_to_file(const QString & name,
                               const QMap<double, double> & map_x,
                               const QMap<double, double> & map_y,
                               const QMap<double, double> & map_phi,
                               const Doub params[])
{
    qDebug() << "writing to file" << name;


    QFile file(name);
    QTextStream out(&file);
    file.open(QIODevice::WriteOnly);
    out << "#";
    for (Int i=0;i<10;i++)
        out << "params["<<i<<"] = " <<  params[i] << " ";
    out << endl;
    out << "r\tx\ty\tphi" << endl;
    QMapIterator<double,double> x(map_x);
    QMapIterator<double,double> y(map_y);
    QMapIterator<double,double> phi(map_phi);
    switch(v) {
        case 1:
        while (x.hasNext()) {
            x.next();
            out << x.key() << "\t" << x.value() << endl;
        }
        break;
        case 4:
        if (map_x.size() > 0 && map_y.size() == 0 && map_phi.size() == 0) {
            while (x.hasNext()) {
                x.next();
                out << x.key() << "\t" << x.value() << endl;
            }
        }
        else if (map_x.size() > 0 && map_y.size() > 0 && map_phi.size() > 0) {
            while (x.hasNext() && y.hasNext() && phi.hasNext()) {
                x.next();
                y.next();
                phi.next();
                //qDebug() << x.key() << "\t" << x.value() << "\t" << y.value() << "\t" << phi.value();
                out << x.key() << "\t" << x.value() << "\t" << y.value() << "\t" << phi.value() << endl;
            }
        }
        break;
    }
    file.close();
    qDebug () << "done.";
}


//-----------------------------------------------------------------------------


void Calculator::diffusion(VecDoub_I &msd, const Int & samples, Doub & sigma)
{
    Doub mu,sum;
    VecDoub var(samples,0.0);

    sum = 0;
    for(Int i=0;i<samples;i++) {
        //qDebug() << "msd["<<i<<"]" << msd[i];
        sum += msd[i];
    }
    mu = sum/samples;
    //qDebug() << "mu:" << mu;

    for(Int i=0;i<samples;i++) {
        var[i] = SQR(msd[i])-SQR(mu);
        //qDebug() << "var["<<i<<"]" << var[i];
    }
    sum = 0;
    for(Int j=0;j<samples;j++) {
        sum += var[j];
    }

    sigma = sum/samples;

    //qDebug() << "sigma:" << sigma;
}

/*
 * This function is kinda redundant right now, but will be needed later
 */
void Calculator::gen_gnuplot(const QString & name)
{

    qDebug() << "generating gnuplot file";

    QFile file(name);
    QTextStream out(&file);
    file.open(QIODevice::WriteOnly);

    out << "L = 2*pi;";
    out << "unset key;";
    out << "set lmargin 10;";
    out << "set rmargin 3;";
    out << "set multiplot;";
    out << "set tmargin = 3;";
    out << "set bmargin = 0;";
    out << "set border 2+4+8;";
    out << "set size 1,0.66667;";
    out << "set origin 0.0,0.33333;";
    out << "set xtics;";
    out << "set format x \"\";";
    out << "unset xlabel;";
    out << "set ylabel \"x(kT) mod L \";";
    out << "set ytics (\"0\" 0, \"L/2\", \"L\" L);";
    out << "set yrange [0:L];";
    out << "plot \""<<file_rk_bif<<"\" w dots 1;";
    out << "unset title;";
    out << "set tmargin 0;";
    out << "set bmargin 3;";
    out << "unset format;";
    out << "set size 1,0.33333;";
    out << "set border 1+2+4+8;";
    out << "set origin 0.0,0.0;";
    out << "set xtics 0,0.1;";
    out << "set xlabel \"a\";";
    out << "set ylabel \"v\";";
    out << "set yrange [-0.75:0.75];";
    out << "set ytics -0.5,0.5;";
    out << "plot \""<<file_rk_avg<<"\" w dots 3;";
    out << "unset multiplot;";

}

void Calculator::filter(QMap<double, double> & map, const Doub & eps)
{
    qDebug() << "filter crap smaller than " << eps;
    Int size, k;
    Doub diff, key;
    QList<double> values, list;
    list = map.uniqueKeys();
    //qDebug() << list;
    size = list.size();
    //qDebug() << "size of map:" << size;
    for(Int i = 0; i < size; i++) {
        key = list.at(i);
        //qDebug() << "["<<key<<"]" << "writing values into list";
        values = map.values(key);
        //qDebug() << "["<<key<<"]" << values.size() << "values for" << key;
        k = 0;
        for(Int k=0;k<values.size()-1;k++) {
            diff = abs(values.at(k+1) - values.at(k));
            //qDebug() << values.at(k+1) <<"-"<< values.at(k) << "=" << diff;
            if (diff <= eps) {
                //qDebug() << diff << "<=" << eps;
                //qDebug() << "["<<k<<"]" << "removing" << values.at(k) << "from list";
                values.replace(k+1,values.at(k));
            }
        } // end for
        for (int j = 0; j < values.size(); j++)
            //qDebug() << "["<<j<<"]" << values.at(j);
        //qDebug() << "removing" << key << "from map";
        map.remove(key);
        for (int j = 0; j < values.size(); j++) {
            //qDebug() << "inserting" << values.at(j) << "into map at" << key;
            map.insertMulti(key,values.at(j));
        }
        //qDebug() << map.values(key);
        //qDebug() << endl << "end while" << endl;
    }
}

void  Calculator::IC_setup(VecDoub &y, Doub x0, Doub y0, Doub xdot0, Doub ydot0,
                          Doub phi0, Doub params[])
{
    //Int n = y.size();
    const Int sys = c->valuesMap.value(1);

    /*
    qDebug() << "using " << "\tx0 = " << x0 << "\ty0 = " << y0
            << "\txdot0 = " << xdot0 << "\tydot0 = " << ydot0
            << "\tphi0 = " << fmod(phi0,360) << endl;
    */

    switch(sys) {
        case 1:
        params[0] = c->valuesMap.value(2); // a;
        params[1] = c->valuesMap.value(3); // A;
        params[2] = c->valuesMap.value(4); // F;
        params[3] = c->valuesMap.value(5); // l;
        params[4] = c->valuesMap.value(6); // w;
        params[5] = c->valuesMap.value(25); // eta1;
        params[6] = 0;
        params[9] = c->valuesMap.value(32); // T;
        params[10] = 0;
        y[0] = x0, y[1] = xdot0;
        break;
        case 4:
        params[0] = c->valuesMap.value(2); // a;
        params[1] = fmod(c->valuesMap.value(3),360)*PI/180; // alpha;
        params[2] = c->valuesMap.value(4); // F;
        params[3] = c->valuesMap.value(5); // l;
        params[4] = c->valuesMap.value(6); // w;
        params[5] = c->valuesMap.value(25); // eta1;
        params[6] = c->valuesMap.value(26); // eta2;      
        params[9] = c->valuesMap.value(32); // T;
        params[10] = fmod(c->valuesMap.value(40),360)*PI/180; // theta;
        params[17] = fmod(c->valuesMap.value(41),360)*PI/180; // psi;
        params[18] = c->valuesMap.value(42); // U;
        Doub e0 = 1.0/(params[5]+params[6]);
        Doub e1 = 1.0/(params[5]*params[3]);
        Doub e2 = 1.0/(params[6]*params[3]);
        Doub e3 = 1.0/params[3]*((1.0/params[5])-(1.0/params[6]));
        Doub fx = cos(params[10])*params[2];
        Doub fy = sin(params[10])*params[2];
        params[11] = e0; // 1/eta
        params[12] = e1;
        params[13] = e2;
        params[14] = e3;
        params[15] = fx;
        params[16] = fy;
        params[7] = params[3]*params[11]*params[5]; // l1;
        params[8] = params[3]*params[11]*params[6]; // l2;
        y[0] = x0, y[1] = y0, y[2] = phi0;
        break;
    }
    /*
    for (Int i = 0; i<n;i++)
        qDebug() << "y("<<i<<") =" << y[i];
    qDebug() << "eta1 = " << eta1 << "\teta2 = " << eta2 << "\tA = " << A << "\tF = " << F;
    */
}

void Calculator::bifparam(const int &v, const QString &param, const Doub &r,
                          Doub params[])
{
    switch(v) {
        case 1:
        if (param == "E")
            params[5] = r;
        else if (param == "F")
            params[2] = r;
        else if (param == "A")
            params[1] = r;
        else
            throw ("Fatal error!");
        break;
        case 4:
        if (param == "A")
            params[1] = fmod(r,360)*PI/180;
        else if (param == "F") {
            params[2] = r;
            params[15] = cos(params[10])*params[2];
            params[16] = sin(params[10])*params[2];
        }
        else if (param == "a")
            params[0] = r;
        else if (param == "w")
            params[4] = r;
        else if (param == "l") {
            params[3] = r;
            params[11] = 1.0/(params[5]+params[6]);
            params[12] = 1.0/(params[5]*params[3]);
            params[13] = 1.0/(params[6]*params[3]);
            params[14] = 1.0/params[3]*((1.0/params[5])-(1.0/params[6]));
            params[7] = params[3]*params[11]*params[5]; // l1;
            params[8] = params[3]*params[11]*params[6]; // l2;
        }
        else if (param == "e") {
            params[5] = r;
            params[11] = 1.0/(params[5]+params[6]);
            params[12] = 1.0/(params[5]*params[3]);
            params[13] = 1.0/(params[6]*params[3]);
            params[14] = 1.0/params[3]*((1.0/params[5])-(1.0/params[6]));
            params[7] = params[3]*params[11]*params[5]; // l1;
            params[8] = params[3]*params[11]*params[6]; // l2;
        }
        else if (param == "t") {
            params[10] = fmod(r,360)*PI/180;
            params[15] = cos(params[10])*params[2];
            params[16] = sin(params[10])*params[2];
        }
        else if (param == "T") {
            params[9] = r;
        }
        else
            throw("Fatal error!");
        break;
        qDebug() << "Bifurcation parameter is " << param;
    }
}

void Calculator::set_color(const Doub vel, QString & color)
{
    Doub ctol = 0.1;
    if (vel > 1.0-ctol && vel < 1.0+ctol)
        color = "red";
    else if (vel > 0.5-ctol && vel < 0.5+ctol)
        color = "orange";
    else if (vel > 0.0-ctol && vel < 0.0+ctol)
        color = "grey";
    else if (vel > -0.5-ctol && vel < -0.5+ctol)
        color = "green";
    else if (vel > -1.0-ctol && vel < -1.0+ctol)
        color = "blue";
    else color = "black";
}

void Calculator::calc2(const Int n)
{
    // setting up a few start values
    qDebug() << "calc2 has been called: " << "calc2("<<n<<")";
    Normaldev gauss(0,1,time(NULL));
    Ran ran(time(NULL));
    Doub x1,x2,y1,y2,xtest;
    Doub L, tau, sigma;
    Doub x0, xdot0, y0, ydot0, phi0;
    x0 = c->valuesMap.value(8);
    xdot0 = c->valuesMap.value(10);
    ydot0 = c->valuesMap.value(24);
    y0 = c->valuesMap.value(23);
    phi0 = fmod(c->valuesMap.value(27),360)*PI/180;
    const Int samples = c->valuesMap.value(35);
    const Int v = c->valuesMap.value(1);
    const Int M = c->valuesMap.value(11);
    const Doub fromt = c->valuesMap.value(22);
    const Int s = c->valuesMap.value(14);
    const Doub h = c->valuesMap.value(13);
    const Doub t0 = c->valuesMap.value(7);
    const Doub fromr1 = c->valuesMap.value(17);
    const Doub fromr2 = c->valuesMap.value(37);
    const Doub tor1 = c->valuesMap.value(18);
    const Doub tor2 = c->valuesMap.value(38);
    const Doub stepr1 = c->valuesMap.value(19);
    const Doub stepr2 = c->valuesMap.value(39);
    const Doub eps = c->valuesMap.value(36);
    const Doub from = c->valuesMap.value(15);
    const Doub to = c->valuesMap.value(16);
    const Doub hstep = c->valuesMap.value(43);
    Doub params[256];
    //const Doub L = 1;
    const bool ruk = c->ruk;
    const bool eul = c->eul;
    QString param1 = c->str1;
    QString param2 = c->str2;
    qDebug() << "System" << v;

    Doub x, hnew;
    Doub trajectory_length, deflection, velocity_angle, bias_angle;
    Int k, hit;

    Doub xpi[samples][s];
    Doub ypi[samples][s];
    Doub phipi[samples][s];
    VecDoub y(n,0.0);
    VecDoub dydx(n,0.0);
    VecDoub dW(n, 0.0);
    VecDoub yout(n,0.0);
    VecDoub avg(n,0.0);

    if (ruk) {
        switch(M) {
            case 0:
            qDebug() << "standard mode";
            qDebug() << "stepsize:" << h;
            qDebug() << "number of steps:" << s;
            x = t0;
            IC_setup(y,x0,xdot0,y0,ydot0,phi0,params);
            qDebug() << phi0 << y[2];
            k = 0;
            L = 2*PI;
            tau = 2*PI/params[4];
            while(k < s) {
                xtest = x+h;
                if (xtest >= k*tau) {
                    k++;
                }
                derivs(x,y,dydx,params); // sets first dydx[] for rk4()
                rk4(y,dydx,x,h,yout,params);
                for (Int j=0;j<n;j++)
                    y[j] = yout[j];
                x += h;
                if (k >= s*fromt) {
                    switch(v) {
                        case 1:
                        rkValues_x.insert(x,yout[0]);
                        break;
                        case 4:
                        //xpi = ABS(fmod(yout[0],(2*PI)));
                        //ypi = ABS(fmod(yout[1],(2*PI)));
                        rkValues_x.insert(x,yout[0]);
                        rkValues_y.insert(x,yout[1]);
                        //rkValues_x.insert(x,xpi);
                        //rkValues_y.insert(x,ypi);
                        rkValues_phi.insert(x,(180/PI)*ABS(fmod(yout[2],(2*PI))));
                        x1 = y[0] + params[7]*cos(yout[2]);
                        y1 = y[1] + params[7]*sin(yout[2]);
                        x2 = y[0] - params[8]*cos(yout[2]);
                        y2 = y[1] - params[8]*sin(yout[2]);
                        rkValues_p1x.insert(x,x1);
                        rkValues_p1y.insert(x,y1);
                        rkValues_p2x.insert(x,x2);
                        rkValues_p2y.insert(x,y2);
                        rkValues_vx.insert(x,dydx[0]);
                        rkValues_vy.insert(x,dydx[1]);
                        rkValues_vphi.insert(x,dydx[2]);
                        break;
                    }
                }
            } // end while
            qDebug() << "sizeof(rkValues_x) =" << rkValues_x.size();
            qDebug() << "sizeof(rkValues_y) =" << rkValues_y.size();
            qDebug() << "sizeof(rkValues_phi) =" << rkValues_phi.size();
            write_to_file(file_rk_out, rkValues_x, rkValues_y, rkValues_phi, params);
            write_to_file(file_rk_pt1, rkValues_p1x, rkValues_p1y, rkValues_phi, params);
            write_to_file(file_rk_pt2, rkValues_p2x, rkValues_p2y, rkValues_phi, params);
            write_to_file(file_rk_vel, rkValues_vx, rkValues_vy, rkValues_vphi, params);
            break;
            case 1:
            qDebug() << "converge mode";
            Int iter;
            Doub hh, ss;
            for (iter=1;from+(iter*hstep)<=to;iter++) {
                for (Int z=0;z<samples;z++) {
                    qDebug() << "at step" << z << "of" << samples;
                    x = t0;
                    hh = from+(iter*hstep);
                    ss = (s/hh);
                    qDebug() << "stepsize is" << hh << "with" << ss << "total steps.";
                    //x0 = randpi(ran), xdot0 = randpi(ran), y0 = randpi(ran), ydot0 = randpi(ran), phi0 = randpi(ran);
                    IC_setup(y,x0,xdot0,y0,ydot0,phi0,params);
                    k = 0;
                    L = 2*PI;
                    tau = 2*PI/params[4];
                    while(k < s) {
                        //qDebug() << k << "<" << s;
                        //qDebug() << k << tau << k*tau;
                        xtest = x+hh;
                        if (xtest >= k*tau) {
                            //qDebug() << xtest << ">=" << k << "*" << tau << "("<<k*tau<<")";
                            hnew = k*tau-x;
                            derivs(x,y,dydx,params);
                            rk4(y,dydx,x,hnew,yout,params);
                            k++;
                            x += hnew;
                            //qDebug() << x;
                        } else {
                            //qDebug() << xtest << "=" << x<<"+"<<hh;
                            derivs(x,y,dydx,params);
                            rk4(y,dydx,x,hh,yout,params); // xold
                            x += hh;
                        }
                        for (Int j=0;j<n;j++)
                            y[j] = yout[j];
                    } // end while
                    switch(v) {
                        case 1:
                        rkValues_x.insert(hh,yout[0]);
                        break;
                        case 4:
                        rkValues_x.insert(hh,yout[0]);
                        rkValues_y.insert(hh,yout[1]);
                        rkValues_phi.insert(hh,yout[2]);
                        break;
                    }
                    x = t0;
                    //x0 = randpi(ran), xdot0 = randpi(ran), y0 = randpi(ran), ydot0 = randpi(ran), phi0 = randpi(ran);
                    IC_setup(y,x0,xdot0,y0,ydot0,phi0,params);
                    k = 0;
                    L = 2*PI;
                    tau = 2*PI/params[4];
                    while (k < s) {
                        xtest = x+hh;
                        if (xtest >= k*tau) {
                            hnew = k*tau-x;
                            derivs(x,y,dydx,params);
                            derivw(x,y,dW,params,gauss);
                            euler(y,dydx,dW,x,hnew,yout);
                            k++;
                            x += hnew;
                        } else {
                            derivs(x,y,dydx,params);
                            derivw(x,y,dW,params,gauss);
                            euler(y,dydx,dW,x,hh,yout);
                            x += hh;
                        }
                        for (Int j=0;j<n;j++)
                            y[j] = yout[j];
                    } // end while
                    switch(v) {
                        case 1:
                        euValues_x.insert(hh,yout[0]);
                        break;
                        case 4:
                        euValues_x.insert(hh,yout[0]);
                        euValues_y.insert(hh,yout[1]);
                        euValues_phi.insert(hh,yout[2]);
                        break;
                    } // end switch
                } // end samples
            } // end for
            qDebug() << "sizeof(rkValues_x) =" << rkValues_x.size();
            write_to_file(file_rk_conv, rkValues_x, rkValues_y, rkValues_phi, params);
            write_to_file(file_eu_conv, euValues_x, euValues_y, euValues_phi, params);
            break;
            case 2:
            qDebug() << "bifurcation mode";
            qDebug() << "bifurcation parameter: " << param1;
            qDebug() << "Number of samples : " << samples;
            Doub r;
            for (Int j=0;(j*stepr1)<=(tor1-fromr1);j++) {
                r = fromr1+j*stepr1;
                qDebug() << "at step" << j << "of" << (tor1-fromr1)/stepr1;
                for (Int z=0;z<samples;z++) {
                    x = t0;
                    x0 = randpi(ran), xdot0 = randpi(ran), y0 = randpi(ran), ydot0 = randpi(ran), phi0 = randpi(ran);
                    //qDebug() << "sample" << z << "using" << x0 << xdot0 << y0 << ydot0 << phi0;
                    IC_setup(y,x0,xdot0,y0,ydot0,phi0,params);
                    bifparam(v,param1,r,params);
                    //qDebug() << x << y[0] << y[1] << y[2];
                    //for(Int i = 0;i<=10;i++)
                      //  qDebug() << params[i];
                    // inner for: iterate over runge-kutta
                    k = 0;
                    L = 2*PI;
                    tau = 2*PI/params[4];
                    Doub hnew;
                    //qDebug() << "tau: " << tau << "\tL: " << L;
                    while (k<s) {
                        xtest = x+h; // test next x increment
                        //if (xtest < k*tau) { qDebug() << "["<<k<<"]" << xtest << "<" << k*tau << "..."; }
                        if (xtest >= k*tau) {
                            //qDebug() << "["<<k<<"]" << xtest << ">" << k*tau << "!";
                            //qDebug() << "new h is" << k << "*" << tau << "-" << x << "=" << hnew;
                            hnew = k*tau-x;
                            //qDebug() << "making rk4() with stepsize"<<hnew<<"at"<<r;
                            derivs(x,y,dydx,params);
                            rk4(y,dydx,x,hnew,yout,params);
                            switch(v) {
                                case 1:
                                xpi[z][k] = ABS(fmod(yout[0],(2*PI)));
                                //qDebug() << "xpi["<<z<<"]["<<k<<"]:" << xpi[z][k];
                                if (k >= s*fromt) {
                                    rkValues_x.insert(r,xpi[z][k]);
                                    rkValues_xdot.insert(r,dydx[0]);
                                    }
                                break;
                                case 4:
                                xpi[z][k] = yout[0];
                                //qDebug() << "xpi["<<z<<"]["<<k<<"]:" << xpi[z][k];
                                ypi[z][k] = yout[1];
                                phipi[z][k] = yout[2];
                                rkDist_x.insert(r,SQR(yout[0]-x0));
                                rkDist_y.insert(r,SQR(yout[1]-y0));
                                rkDist_phi.insert(r,SQR(yout[2]-phi0));
                                if (k >= s*fromt) {
                                    rkValues_x.insert(r,ABS(fmod(xpi[z][k],L)));
                                    rkValues_y.insert(r,ABS(fmod(ypi[z][k],L)));
                                    rkValues_phi.insert(r,ABS(fmod(phipi[z][k],L)));
                                    rkValues_xdot.insert(r,dydx[0]);
                                    rkValues_ydot.insert(r,dydx[1]);
                                    rkValues_phidot.insert(r,dydx[2]);
                                }
                                break;
                            } // end switch
                            k++;
                        }
                        derivs(x,y,dydx,params);
                        rk4(y,dydx,x,h,yout,params); // xold
                        for (Int j=0;j<n;j++)
                            y[j] = yout[j]; // y[x+h] !!!!!!!
                        x += h; // do it for real
                    } // end while
                    //qDebug() << "at" << x << "in time for k = " << k;
                    switch(v) {
                        Int m_x, m_y, m_phi;
                        case 1:
                        m_x = yout[0]/L;
                        avg[0] = (Doub) m_x/k;
                        rkAvgvelo_x.insert(r,avg[0]);
                        break;
                        case 4:
                        m_x = yout[0]/L; m_y = yout[1]/L; m_phi = yout[2]/L;
                        avg[0] = (Doub) m_x/k; avg[1] = (Doub) m_y/k; avg[2] = (Doub) m_phi/k;
                        // makes only sense with r = params[10]
                        trajectory_length = sqrt(SQR(yout[0]-x0)+SQR(yout[1])-y0);
                        if (abs(yout[0]-x0) > trajectory_length) {
                            velocity_angle = PI;
                        } else {
                            velocity_angle = (acos((yout[0]-x0)/trajectory_length));
                        }
                        bias_angle = params[10];
                        deflection = (180/PI)*ABS(velocity_angle - bias_angle);
                        /*
                        qDebug() << velocity_angle << "("<<yout[0]-x0<<")"
                                << "-" << bias_angle << "="
                                << deflection
                                << "[ length:"<<trajectory_length<<"]";                     
                        */
                        rkAvgvelo_x.insert(r,avg[0]);
                        rkAvgvelo_y.insert(r,avg[1]);
                        rkAvgvelo_phi.insert(r,avg[2]);
                        rkDefAngle.insert(r,deflection);
                        break;
                    } // end switch
                } // samples loop
                VecDoub msd_x(samples,0.0);
                VecDoub msd_y(samples,0.0);
                VecDoub msd_phi(samples,0.0);
                Doub sigma_x[s-1];
                Doub sigma_y[s-1];
                Doub sigma_phi[s-1];
                //qDebug() << "samples:" << samples << "steps:" << s;
                for(Int i=0;i<s;i++) {
                    //qDebug() << "["<<i<<"]:---------------------------------";
                    for(Int j=0;j<samples;j++) {
                        //qDebug() << "xpi["<<j<<"]["<<i<<"]:" << xpi[j][i];
                        msd_x[j] = xpi[j][i];
                        //qDebug() << "["<<j<<"]:" << sper_x[j];
                        msd_y[j] = ypi[j][i];
                        msd_phi[j] = phipi[j][i];
                    }
                    diffusion(msd_x, samples, sigma);
                    sigma_x[i] = sigma;
                    diffusion(msd_y, samples, sigma);
                    sigma_y[i] = sigma;
                    diffusion(msd_phi, samples, sigma);
                    sigma_phi[i] = sigma;
                    //qDebug() << "sigma_x["<<i<<"]:" << sigma_x[i];
                }
                // the lists of keys in QMultiMap are read out backwards...
                for(Int k=(s-1);k>=0;k--) {
                    rkVariance_x.insert(r,sigma_x[k]);
                    rkVariance_y.insert(r,sigma_y[k]);
                    rkVariance_phi.insert(r,sigma_phi[k]);
                } 
               //qDebug() << rkVariance_x;
            } // end outer for
            /*
             * we need to do something about this
             */
            /*
            filter(rkValues_x,eps);
            filter(rkValues_y,eps);
            filter(rkValues_xdot,eps);
            filter(rkValues_ydot,eps);
            filter(rkAvgvelo_x, eps);
            filter(rkAvgvelo_y, eps);
            filter(rkAvgvelo_phi,eps);*/
            qDebug() << "sizeof(rkValues_x) =" << rkValues_x.size();
            qDebug() << "sizeof(rkValues_y) =" << rkValues_y.size();
            qDebug() << "sizeof(rkValues_xdot) =" << rkValues_xdot.size();
            qDebug() << "sizeof(rkValues_ydot) =" << rkValues_ydot.size();
            write_to_file(file_rk_bif, rkValues_x, rkValues_y, rkValues_phi, params);
            write_to_file(file_rk_biv, rkValues_xdot, rkValues_ydot, rkValues_phidot, params);
            write_to_file(file_rk_avg, rkAvgvelo_x, rkAvgvelo_y, rkAvgvelo_phi, params);
            write_to_file(file_rk_variance, rkVariance_x, rkVariance_y, rkVariance_phi,params);
            write_to_file(file_rk_distance, rkDist_x, rkDist_y, rkDist_phi, params);
            write_to_file(file_rk_defangle, rkDefAngle, NullMap, NullMap, params);
            break;
            case 3:
            qDebug() << "dual bifurcation mode";
            qDebug() << "bifurcation parameters: " << param1<<"\t"<<param2;
            qDebug() << "Number of samples : " << samples;
            Doub stp1, stp2;
            QString vx_color, vy_color;
            QString name = "tmp.dat";
            QFile file(name);
            QTextStream out(&file);
            file.open(QIODevice::WriteOnly);
            out << "x\ty\tvx\tvx_color\tvy\tvy_color\tvphi" << endl;
            for(Int it1=0;(it1*stepr1)<=(tor1-fromr1);it1++) {
                for(Int it2=0;(it2*stepr2)<=(tor2-fromr2);it2++) {
                    stp1 = fromr1+it1*stepr1;
                    stp2 = fromr2+it2*stepr2;
                    qDebug() << stp1 << "("<<tor1<<")" << stp2 << "("<<tor2<<")";
                    for (Int z=0;z<samples;z++) {
                        x = t0;
                        x0 = randpi(ran), xdot0 = randpi(ran), y0 = randpi(ran), ydot0 = randpi(ran), phi0 = randpi(ran);
                        IC_setup(y,x0,xdot0,y0,ydot0,phi0,params);
                        bifparam(v,param1,stp1,params);
                        bifparam(v,param2,stp2,params);
                        /*
                        out << "# ";
                        for (Int i=0;i<10;i++)
                            out << "params["<<i<<"] = " <<  params[i] << " ";
                        out << endl;
                        */
                        // inner for: iterate over runge-kutta
                        k = 0;
                        L = 2*PI;
                        tau = 2*PI/params[4];
                        Doub hnew;
                        //qDebug() << "tau: " << tau << "\tL: " << L;
                        while (k<s) {
                            xtest = x+h; // test next x increment
                            //if (xtest < k*tau) { qDebug() << "["<<k<<"]" << xtest << "<" << k*tau << "..."; }
                            if (xtest >= k*tau) {
                                //qDebug() << "["<<k<<"]" << xtest << ">" << k*tau << "!";
                                //qDebug() << "new h is" << k << "*" << tau << "-" << x << "=" << hnew;
                                hnew = k*tau-x;
                                //qDebug() << "making rk4() with stepsize"<<hnew<<"at"<<r;
                                derivs(x,y,dydx,params);
                                rk4(y,dydx,x,hnew,yout,params);
                                k++;
                            }
                            derivs(x,y,dydx,params);
                            rk4(y,dydx,x,h,yout,params); // xold
                            for (Int j=0;j<n;j++)
                                y[j] = yout[j]; // y[x+h] !!!!!!!
                            x += h; // do it for real
                        } // end while
                        Int m_x, m_y, m_phi;
                        m_x = yout[0]/L; m_y = yout[1]/L; m_phi = yout[2]/L;
                        avg[0] = (Doub) m_x/k; avg[1] = (Doub) m_y/k;
                        avg[2] = (Doub) m_phi/k;
                        set_color(avg[0], vx_color);
                        set_color(avg[1], vy_color);
                        out << stp1 << "\t" << stp2 << "\t"
                                << avg[0] <<  "\t" << vx_color << "\t" << avg[1]
                                << "\t" << vy_color << "\t" << avg[2] << endl;
                    } // end samples
                } // end 2. outer for
            } // end 1. outer for
            file.close();
            qDebug() << "moving" << name << "to safety";
            file.copy(name, file_rk_dualbif);
            break;
        } // end switch
    } // end if

    if (eul) {
        switch(M) {
          case 0:
          qDebug() << "(euler) standard mode";
          qDebug() << "stepsize:" << h;
          qDebug() << "number of steps:" << s;
          x = t0;
          IC_setup(y,x0,xdot0,y0,ydot0,phi0,params);
          k = 0;
          hit = 0;
          L = 2*PI;
          tau = 2*PI/params[4];
          while(k < s) {
              //qDebug() << k << "<" << s;
              //qDebug() << "x is now" << x;
              //qDebug() << "deriving values at" << x;
              //qDebug() << x0, y0, phi0;
              derivs(x,y,dydx,params); // sets first dydx[] for rk4()
              derivw(x,y,dW,params,gauss);
              euler(y,dydx,dW,x,h,yout);
              //for (Int i=0;i<=18;i++)
                //qDebug() << "params["<<i<<"]" << params[i];
              if (x >= hit*0.1) {
                  //qDebug() << x <<"=="<< hit*0.1;
                  //qDebug() << "writing2 to map";
                  hit++;
                  //qDebug() << "next value to write out" << hit*0.1;
                  if (k >= s*fromt) {
                      switch(v) {
                      case 1:
                          euValues_x.insert(x,yout[0]);
                      break;
                      case 4:
                        //xpi = ABS(fmod(yout[0],(2*PI)));
                        //ypi = ABS(fmod(yout[1],(2*PI)));
                        euValues_x.insert(x,yout[0]);
                        euValues_y.insert(x,yout[1]);
                        //rkValues_x.insert(x,xpi);
                        //rkValues_y.insert(x,ypi);
                        euValues_phi.insert(x,yout[2]);
                        x1 = yout[0] + params[7]*cos(yout[2]);
                        y1 = yout[1] + params[7]*sin(yout[2]);
                        x2 = yout[0] - params[8]*cos(yout[2]);
                        y2 = yout[1] - params[8]*sin(yout[2]);
                        euValues_p1x.insert(x,x1);
                        euValues_p1y.insert(x,y1);
                        euValues_p2x.insert(x,x2);
                        euValues_p2y.insert(x,y2);
                        euValues_vx.insert(x,dydx[0]);
                        euValues_vy.insert(x,dydx[1]);
                        euValues_vphi.insert(x,dydx[2]);
                        break;
                    } // end fromt switch
                  } // end fromt if
              } // end hit if
              //qDebug() << "making step of size" << h;
              for (Int j=0;j<n;j++)
                  y[j] = yout[j];
              x += h;
              //qDebug() << "new x is now" << x;
              if (x >= k*tau) {
                  //qDebug() << x << ">=" << k*tau;
                  //qDebug() << "incrementing k";
                  k++;
                  //qDebug() << "new value of x needed is" << k*tau;
              }
          } // end while
          qDebug() << "sizeof(euValues_x) =" << euValues_x.size();
          qDebug() << "sizeof(euValues_y) =" << euValues_y.size();
          qDebug() << "sizeof(euValues_phi) =" << euValues_phi.size();
          write_to_file(file_eu_out, euValues_x, euValues_y, euValues_phi, params);
          write_to_file(file_eu_pt1, euValues_p1x, euValues_p1y, euValues_phi, params);
          write_to_file(file_eu_pt2, euValues_p2x, euValues_p2y, euValues_phi, params);
          write_to_file(file_eu_vel, euValues_vx, euValues_vy, euValues_vphi, params);
          break;
          case 2:
          qDebug() << "bifurcation mode";
          qDebug() << "bifurcation parameter: " << param1;
          qDebug() << "Number of samples : " << samples;
          Doub r;
          for (Int j=0;(j*stepr1)<=(tor1-fromr1);j++) {
              r = fromr1+j*stepr1;
              qDebug() << "at step" << j << "of" << (tor1-fromr1)/stepr1;
              for (Int z=0;z<samples;z++) {
                  x = t0;
                  x0 = randpi(ran), xdot0 = randpi(ran), y0 = randpi(ran), ydot0 = randpi(ran), phi0 = randpi(ran);
                  //qDebug() << "sample" << z << "using" << x0 << xdot0 << y0 << ydot0 << phi0;
                  IC_setup(y,x0,xdot0,y0,ydot0,phi0,params);
                  bifparam(v,param1,r,params);
                  // inner for: iterate over runge-kutta
                  k = 0;
                  L = 2*PI;
                  tau = 2*PI/params[4];
                  Doub hnew;
                  //qDebug() << "tau: " << tau << "\tL: " << L;
                  while (k<s) {
                      xtest = x+h; // test next x increment
                      //if (xtest < k*tau) { qDebug() << "["<<k<<"]" << xtest << "<" << k*tau << "..."; }
                      if (xtest >= k*tau) {
                          //qDebug() << "["<<k<<"]" << xtest << ">" << k*tau << "!";
                          //qDebug() << "new h is" << k << "*" << tau << "-" << x << "=" << hnew;
                          hnew = k*tau-x;
                          //qDebug() << "making euler() with stepsize"<<hnew<<"at"<<r;
                          derivs(x,y,dydx,params);
                          derivw(x,y, dW, params,gauss);
                          euler(y,dydx,dW,x,hnew,yout);
                          switch(v) {
                          case 1:
                              xpi[z][k] = yout[0];
                              if (k >= s*fromt) {
                                  euValues_x.insert(r,ABS(fmod(xpi[z][k],L)));
                                  euValues_xdot.insert(r,dydx[0]);
                              }
                              break;
                          case 4:
                              xpi[z][k] = yout[0];
                              //qDebug() << "xpi["<<z<<"]["<<k<<"]:" << xpi[z][k];
                              ypi[z][k] = yout[1];
                              phipi[z][k] = yout[2];
                              euDist_x.insert(r,SQR(yout[0]-x0));
                              euDist_y.insert(r,SQR(yout[1]-y0));
                              euDist_phi.insert(r,SQR(yout[2]-phi0));
                              if (k >= s*fromt) {
                                  euValues_x.insert(r,ABS(fmod(xpi[z][k],L)));
                                  euValues_y.insert(r,ABS(fmod(ypi[z][k],L)));
                                  euValues_phi.insert(r,ABS(fmod(phipi[z][k],L)));
                                  euValues_xdot.insert(r,dydx[0]);
                                  euValues_ydot.insert(r,dydx[1]);
                                  euValues_phidot.insert(r,dydx[2]);
                              }
                              break;
                          } // end switch
                          k++;
                      }
                      derivs(x,y,dydx,params);
                      derivw(x,y,dW, params,gauss);
                      euler(y,dydx,dW,x,h,yout); // xold
                      for (Int j=0;j<n;j++)
                          y[j] = yout[j]; // y[x+h] !!!!!!!
                      x += h; // do it for real
                  } // end while
                  //qDebug() << "at" << x << "in time for k = " << k;
                  switch(v) {
                      Int m_x, m_y, m_phi;
                      case 1:
                      m_x = yout[0]/L;
                      avg[0] = (Doub) m_x/k;
                      euAvgvelo_x.insert(r,avg[0]);
                      break;
                      case 4:
                      m_x = yout[0]/L; m_y = yout[1]/L; m_phi = yout[2]/L;
                      avg[0] = (Doub) m_x/k; avg[1] = (Doub) m_y/k; avg[2] = (Doub) m_phi/k;
                      trajectory_length = sqrt(SQR(yout[0]-x0)+SQR(yout[1])-y0);
                      if (abs(yout[0]-x0) > trajectory_length) {
                          velocity_angle = PI;
                      } else {
                          velocity_angle = (acos((yout[0]-x0)/trajectory_length));
                      }
                      bias_angle = params[10];
                      deflection = (180/PI)*ABS(velocity_angle - bias_angle);
                      euAvgvelo_x.insert(r,avg[0]);
                      euAvgvelo_y.insert(r,avg[1]);
                      euAvgvelo_phi.insert(r,avg[2]);
                      euDefAngle.insert(r,deflection);
                      break;
                  } // end switch
              } // samples loop
              VecDoub msd_x(samples,0.0);
              VecDoub msd_y(samples,0.0);
              VecDoub msd_phi(samples,0.0);
              Doub sigma_x[s-1];
              Doub sigma_y[s-1];
              Doub sigma_phi[s-1];
              //qDebug() << "samples:" << samples << "steps:" << s;
              for(Int i=0;i<s;i++) {
                  //qDebug() << "["<<i<<"]:---------------------------------";
                  for(Int j=0;j<samples;j++) {
                      //qDebug() << "xpi["<<j<<"]["<<i<<"]:" << xpi[j][i];
                      msd_x[j] = xpi[j][i];
                      //qDebug() << "["<<j<<"]:" << sper_x[j];
                      msd_y[j] = ypi[j][i];
                      msd_phi[j] = phipi[j][i];
                  }
                  diffusion(msd_x, samples, sigma);
                  sigma_x[i] = sigma;
                  diffusion(msd_y, samples, sigma);
                  sigma_y[i] = sigma;
                  diffusion(msd_phi, samples, sigma);
                  sigma_phi[i] = sigma;
                  //qDebug() << "sigma_x["<<i<<"]:" << sigma_x[i];
              }
              // the lists of keys in QMultiMap are read out backwards...
              for(Int k=(s-1);k>=0;k--) {
                  euVariance_x.insert(r,sigma_x[k]);
                  euVariance_y.insert(r,sigma_y[k]);
                  euVariance_phi.insert(r,sigma_phi[k]);
              }
              //qDebug() << euVariance_x;
          } // end outer for
          /*
           * we need to do something about this
           */
          qDebug() << "sizeof(euValues_x) =" << euValues_x.size();
          qDebug() << "sizeof(euValues_y) =" << euValues_y.size();
          qDebug() << "sizeof(euValues_xdot) =" << euValues_xdot.size();
          qDebug() << "sizeof(euValues_ydot) =" << euValues_ydot.size();
          write_to_file(file_eu_bif, euValues_x, euValues_y, euValues_phi, params);
          write_to_file(file_eu_biv, euValues_xdot, euValues_ydot, euValues_phidot, params);
          write_to_file(file_eu_avg, euAvgvelo_x, euAvgvelo_y, euAvgvelo_phi, params);
          write_to_file(file_eu_variance, euVariance_x, euVariance_y, euVariance_phi,params);
          write_to_file(file_eu_distance, euDist_x, euDist_y, euDist_phi, params);
          write_to_file(file_eu_defangle, euDefAngle, NullMap, NullMap, params);
          break;
          case 3:
          qDebug() << "dual bifurcation mode";
          qDebug() << "bifurcation parameters: " << param1<<"\t"<<param2;
          qDebug() << "Number of samples : " << samples;
          Doub stp1,stp2;
          QString vx_color, vy_color;
          QString name = "tmp.dat";
          QFile file(name);
          QTextStream out(&file);
          file.open(QIODevice::WriteOnly);
          out << "x\ty\tvx\tvx_color\tvy\tvy_color\tvphi" << endl;
          for (Int it1=0;(it1*stepr1)<=(tor1-fromr1);it1++) {
              for (Int it2=0;(it2*stepr2)<=(tor2-fromr2);it2++) {
                  stp1 = fromr1+it1*stepr1;
                  stp2 = fromr2+it2*stepr2;
                  qDebug() << stp1 << "("<<tor1<<")" << stp2 << "("<<tor2<<")";
                  for (Int z=0;z<samples;z++) {
                      x = t0;
                      x0 = randpi(ran), xdot0 = randpi(ran), y0 = randpi(ran), ydot0 = randpi(ran), phi0 = randpi(ran);
                      IC_setup(y,x0,xdot0,y0,ydot0,phi0,params);
                      bifparam(v,param1,stp1,params);
                      bifparam(v,param2,stp2,params);
                      // inner for: iterate over runge-kutta
                      Int k = 0;
                      L = 2*PI;
                      tau = 2*PI/params[4];
                      Doub hnew;
                      //qDebug() << "tau: " << tau << "\tL: " << L;
                      while (k<s) {
                          xtest = x+h; // test next x increment
                          //if (xtest < k*tau) { qDebug() << "["<<k<<"]" << xtest << "<" << k*tau << "..."; }
                          if (xtest >= k*tau) {
                              //qDebug() << "["<<k<<"]" << xtest << ">" << k*tau << "!";
                              //qDebug() << "new h is" << k << "*" << tau << "-" << x << "=" << hnew;
                              hnew = k*tau-x;
                              //qDebug() << "making euler() with stepsize"<<hnew<<"at"<<r;
                              derivs(x,y,dydx,params);
                              derivw(x,y, dW,params,gauss);
                              euler(y,dydx,dW,x,hnew,yout);
                              k++;
                          }
                          derivs(x,y,dydx,params);
                          derivw(x,y, dW,params,gauss);
                          euler(y,dydx,dW,x,h,yout); // xold
                          for (Int j=0;j<n;j++)
                              y[j] = yout[j]; // y[x+h] !!!!!!!
                          x += h; // do it for real
                      } // end while
                        Int m_x, m_y, m_phi;
                        m_x = yout[0]/L; m_y = yout[1]/L; m_phi = yout[2]/L;
                        avg[0] = (Doub) m_x/k; avg[1] = (Doub) m_y/k;
                        avg[2] = (Doub) m_phi/k;
                        set_color(avg[0], vx_color);
                        set_color(avg[1], vy_color);
                        out << stp1 << "\t" << stp2 << "\t"
                                << avg[0] <<  "\t" << vx_color << "\t" << avg[1]
                                << "\t" << vy_color << "\t" << avg[2] << endl;

                  } // end samples
              } // end 2. outer for
          } // end 1. outer for
          file.close();
          qDebug() << "moving" << name << "to safety";
          file.copy(name, file_rk_dualbif);
          break;
        } // end switch
    } // end if

    if (!ruk && !eul)
        throw("No method specified!");

    rkValues_x.clear();
    rkValues_xdot.clear();
    rkValues_y.clear();
    rkValues_ydot.clear();
    rkValues_phi.clear();
    rkValues_phidot.clear();
    rkAvgvelo_x.clear();
    rkAvgvelo_y.clear();
    rkAvgvelo_phi.clear();

    euValues_x.clear();
    euValues_xdot.clear();
    euValues_y.clear();
    euValues_ydot.clear();
    euValues_phi.clear();
    euValues_phidot.clear();
    euAvgvelo_x.clear();
    euAvgvelo_y.clear();
    euAvgvelo_phi.clear();

} // end calculator

//=============================================================================
