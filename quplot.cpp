#include "quplot.h"
#include "ui_quplot.h"
#include "dimdialog.h"
#include "calculator.h"

QuPlot::QuPlot(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::QuPlot)
{
    ui->setupUi(this);
    qDebug() << "MainWindow reporting in";
    dimDialog = 0;
    QFont serifFont("Sans Serif", 9);
    QFont sansFont("Helvetica [Cronyx]", 12);
    QApplication::setFont(serifFont);

    /*
    //buttons
    ui_calcButton = qFindChild<QPushButton*>(this,"calcButton");
    //menus
    ui_menuFile = qFindChild<QMenu*>(this,"menuFile");
    ui_menuStuff = qFindChild<QMenu*>(this,"menuStuff");
    ui_menuHelp = qFindChild<QMenu*>(this,"menuHelp");
    //actions
    ui_actionQuit = qFindChild<QAction*>(this,"actionQuit");
    ui_actionDimension = qFindChild<QAction*>(this,"actionDimension");
    ui_actionAbout = qFindChild<QAction*>(this,"actionAbout");
    //statusbar
    ui_statusBar = qFindChild<QStatusBar*>(this,"statusBar");
*/
}

QuPlot::~QuPlot()
{
    delete ui;
    delete dimDialog;
}

void QuPlot::setDimension()
{
    valuesMap.clear();
    dimDialog = new DimDialog(this);
    qDebug() << "size of DimDialog: " << sizeof(DimDialog);
    dimDialog->show();
    dimDialog->raise();
    dimDialog->activateWindow();
}

void QuPlot::about()
{
    QMessageBox::about(this, tr("About Application"),
                    tr("<b>QuPlot</b> plots nonlinear dynamics<br>"
                    "Written by Rainer 'raw' Wittmaack <wmaack@physik.uni-bielefeld.de><br>"
                    "Many thanks to jniklast <jniklast@web.de>")
                    );
}

void QuPlot::on_actionAbout_triggered()
{
    about();
}

void QuPlot::on_actionDimension_triggered()
{
    setDimension();
}

void QuPlot::on_actionQuit_triggered()
{
    close();
}

void QuPlot::on_calcButton_clicked()
{
   //QString str = valuesMap.value(0);
   int N = valuesMap.value(0);

    qDebug() << "Calculating";
    int v = valuesMap.value(1);
    switch (v) {
    case 1:
        qDebug() << "Damped harmonic oszillator with external force";
        break;
    case 2:
        qDebug() << "Simple damped harmonic oszillator";
        break;
    case 3:
        qDebug() << "Simple harmonic oszillator without damping";
        break;
    case 4:
        qDebug() << "rotating dimer";
        break;
    }

   Calculator m(this);
   //m.calc(N);
   m.calc2(N);
   //close();
}
