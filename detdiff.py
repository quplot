import matplotlib as mpl
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.axes as axe
import matplotlib.collections as collections
import numpy as np
import random
from pylab import *
from matplotlib.pyplot import *

FVAR = '../data/rk_variance.dat'
#FMEAN = '../rk_mean.dat'
FOUT = '../latex/images/diffusion.png'

FIN = []
FIN.append("../rk_out_detdiff1.dat")
FIN.append("../rk_out_detdiff2.dat")
FIN.append("../rk_out_detdiff3.dat")
FIN.append("../rk_out_detdiff4.dat")
FIN.append("../rk_out_detdiff5.dat")

L = 2*np.pi;
tau = L/0.1;
samples = 3

xlabel1 = r'$k\,[\tau]$'
#ylabel1 = r'$\langle {x(k\tau)}^2 - \langle x(k\tau) \rangle^2\rangle$'
ylabel1 = r'$\langle x \rangle$'
xlabel2 = r'$k\,[\tau]$'
ylabel2 = r'$D$'
res = (1024/80,768/80) # default dpi is 80

def get_trajectory(r,x):
	print "---------------------------------------------------------"
	uniquer = unique(r)
	ret_x = [ [] for DUMMYVAR in range(len(uniquer)) ]
	ret_y = [ [] for DUMMYVAR in range(len(uniquer)) ]
	for j in range(len(uniquer)):
		k = 0
		for i in range(len(x)):
			if (r[i] == uniquer[j]):
				k = k+1
				ret_x[j].append(x[i])
				#ret_y[j].append(y[i])
		print uniquer[j],":",len(ret_x[j]),k
	print "---------------------------------------------------------"
	#return (ret_x, ret_y)
	return (ret_x)

print "reading data..."

var  = mlab.csv2rec(FVAR, comments='#', delimiter='\t')
traj = []
for i in range(len(FIN)):
	traj.append(mlab.csv2rec(FIN[i], comments='#', delimiter='\t'))

#mean = mlab.csv2rec(FMEAN, comments='#', delimiter='\t')

r = var.r
var_x = var.x
var_y = var.y
traj_t = []
traj_x = []
traj_y = []
for i in range(len(traj)):
	traj_t.append(traj[i].r)
	traj_x.append(traj[i].x)
	traj_y.append(traj[i].y)

#mean_x = mean.x
#mean_y = mean.y

print "done."

print "extracting trajectories..."
vartra_x = get_trajectory(r,var_x)
vartra_y = get_trajectory(r,var_y)
#meantra_x = get_trajectory(r,mean_x)
#meantra_y = get_trajectory(r,mean_y)
print "done."

t = range(len(vartra_x[0]))
print t
unique_r = unique(r)

fig = plt.figure(figsize=res)
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122)



D_x =[ [] for DUMMYVAR in range(len(unique(r))) ]
D_y =[ [] for DUMMYVAR in range(len(unique(r))) ]

#D_x = []
#D_y = []

#dmin = min(unique(D))
#dmax = max(unique(D))


for i in range(len(FIN)):
	ax1.plot(traj_t[i],traj_x[i])

for i in range(len(unique(r))):
	for j in t:
		tmp = vartra_x[i][j]/(2*(j+1)*tau)
		#print tmp,"=",tra_x[i][j],"/",2*(j+1)*tau
		D_x[i].append(tmp)


for i in range(len(unique(r))):
	for j in t:
		tmp = vartra_y[i][j]/(2*(j+1)*tau)
		D_y[i].append(tmp)

#print tra_x
#print D_x


#for i in range(len(unique(r))):
#	ax1.plot(meantra_x[i],t)

for i in range(len(unique(r))):
	ax2.plot(t,D_x[i])
#	ax2.plot(t,D_y[i])

for i in range(len(unique(r))):
		#tmp_tra = meantra_x[i]
		tmp_D = D_x[i]
		#ax1.text(tmp_tra[max(t)],max(t)+3,unique_r[i],)
		ax2.text(max(t)+3,tmp_D[max(t)],unique_r[i],)

ax1.set_xlim(0,max(traj_t[0]))
#ax1.set_ylim(0,)
ax1.set_xlabel(xlabel1)
ax1.set_ylabel(ylabel1)
#ax2.set_xlim(0,len(tra_x[0]))
ax2.set_ylim(0,2)
ax2.set_xlabel(xlabel2)
ax2.set_ylabel(ylabel2)

plt.savefig(FOUT)
#plt.show()
