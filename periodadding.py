#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import matplotlib as mpl
mpl.rc('text', usetex = True)
import pylab
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.colors as col

FOUT = "../latex/images/periodadding.png"
FIN = []
FIN.append("../rk_out0.dat")
#FIN.append("../rk_out1.dat")
#FIN.append("../rk_out175.dat")
FIN.append("../rk_out2.dat")
#FIN.append("../rk_out225.dat")
#FIN.append("../rk_out23.dat")
FIN.append("../rk_out25.dat")
#FIN.append("../rk_out26.dat")
#FIN.append("../rk_out27.dat")
#FIN.append("../rk_out35.dat")
#FIN.append("../rk_out4.dat")
FIN.append("../rk_out5.dat")
FIN.append("../rk_out6.dat")

L = 2*np.pi
tau = 2*np.pi/0.1
fromt = 80
tot = 90
fs_label = 12
res = (1024/80,768/80)

####################################################################### 

plots = range(len(FIN))
nCols = 3
nRows = len(plots)
nPlots = nCols*len(plots)

print plots

data = []

for i in plots:
	data.append(mlab.csv2rec(FIN[i], delimiter='\t', comments='#'))

t = []
x = []
y = []
phi = []

for i in plots:
	t.append(data[i].r)
	x.append(data[i].x)
	y.append(data[i].y)
	phi.append(data[i].phi)

tstep = t[0][1] - t[0][0]
len_t = len(t[0])
print tstep
print len_t

fromt_eff = fromt/tstep
tot_eff = tot/tstep
tau_eff = tau*tstep

trunc_t = []
trunc_x = []
trunc_y = []
trunc_phi = []

for i in plots:
	tmpt = t[i]
	tmpx = x[i]
	tmpy = y[i]
	tmpphi = phi[i]
	trunc_t.append(tmpt[fromt*tau/tstep:tot*tau/tstep])
	trunc_x.append(tmpx[fromt*tau/tstep:tot*tau/tstep])
	trunc_y.append(tmpy[fromt*tau/tstep:tot*tau/tstep])
	trunc_phi.append(tmpphi[fromt*tau/tstep:tot*tau/tstep])

fig = plt.figure(figsize=res)

ax = [ [] for DUMMYVAR in range(nPlots) ]


for i in range(nPlots):
	ax[i] = fig.add_subplot(nRows,nCols,i+1)

tticks = np.arange(0*tau,100*tau,2*tau)
tticks_eff = []
xticks_eff = []
yticks_eff = []
for i in range(len(tticks)):
	tticks_eff.append(int(tticks[i]))
xticks = np.arange(-100*L,100*L,2*L)
yticks = np.arange(0,10*L,L)
for i in range(len(xticks)):
	xticks_eff.append(int(xticks[i]))
for i in range(len(yticks)):
	yticks_eff.append(int(yticks[i]))
phiticks = np.arange(0,360+1,360/4)

eta_label = [
		r"$\eta_1 = 1.0$",
		r"$\eta_1 = 1.2$",
		#r"$\eta_1 = 1.23$",
		r"$\eta_1 = 1.25$",
		#r"$\eta_1 = 1.27$",
		r"$\eta_1 = 1.5$",
		r"$\eta_1 = 1.6$"
	]

i = 0
j = 0
while j  < nPlots:
	ax[j].plot(t[i],x[i], color='black')
	ax[j+1].plot(x[i],y[i], color='black')
	ax[j+2].plot(t[i],phi[i], color='black')
	i = i+1
	j = j+nCols

j = 0
l = 0
while j  < nPlots:
	ax[j].set_xticks(tticks_eff)
	if (j == 0):
		ax[j].set_yticks(yticks_eff)
	else:
		ax[j].set_yticks(xticks_eff)
	ax[j].set_ylabel(r'$x$')
	if (j == nPlots-3):
		ax[j].set_xlabel(r'$t$')
	ax[j].set_xlim(fromt*tau-1,tot*tau+1)
	ax[j].set_ylim(min(trunc_x[l])-1,max(trunc_x[l])+1)
	l = l+1
	j = j+nCols

j = 1
l = 0
while j <= nPlots:
	if (j == 1):
		ax[j].set_xticks(yticks_eff)
	else:
		ax[j].set_xticks(xticks_eff)
	ax[j].set_yticks(yticks_eff)
	if (j == nPlots-2):
		ax[j].set_xlabel(r'$x$')
	ax[j].set_ylabel(r'$y$')
	ax[j].set_xlim(min(trunc_x[l])-1,max(trunc_x[l])+1)
	ax[j].set_ylim(min(trunc_y[l])-1,max(trunc_y[l])+1)
	l = l+1
	j = j+nCols

j = 2
l = 0
while j <= nPlots:
	ax[j].set_xticks(tticks_eff)
	ax[j].set_yticks(phiticks)
	ax[j].set_xlim(min(trunc_t[l])-1,max(trunc_t[l])+1)
	ax[j].set_ylim(135,315)
	if (j == nPlots-1):
		ax[j].set_xlabel(r'$t$')
	ax[j].set_ylabel(r'$\varphi$')
	l = l+1
	j = j+nCols

for i in range(nPlots/nCols):
	fig.text(0.01,1-0.18-i*0.17,eta_label[i], fontsize=fs_label)

#plt.draw()
#plt.show()
plt.savefig(FOUT)

