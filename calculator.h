//=============================================================================
//
//  CLASS Calculation
//
//=============================================================================

#ifndef CALCULATOR_H
#define CALCULATOR_H


//== INCLUDES =================================================================

#include <QtGui/QDialog>
#include <QFile>
#include <QHash>
#include <QMultiMap>

#include "qp3.h"
#include "dimdialog.h"
#include "quplot.h"
#include "deviates.h"

//== CLASS DEFINITION =========================================================


class Calculator : public QDialog
{
   Q_OBJECT

public:

    // constructor
    explicit Calculator(QuPlot *parent = 0);
    virtual ~Calculator();
    void calc (int N);
    void calc2 (const Int n);

private:
    Doub randpi(Ran &r);

    void write_to_file(const QString & file,
                       const QMap<double, double> & map_x,
                       const QMap<double,double> & map_y,
                       const QMap<double,double> & map_phi,
                       const Doub params[]);
    void derive_init();

    void set_color(const  Doub vel, QString & color);
    void derivs(const Doub x, VecDoub_I &y, VecDoub_O &dydt,
                const Doub params[]);
    void derivw(const Doub x, VecDoub_I &y, VecDoub_O &dW,
                const Doub params[], Normaldev &gauss);
    void rk4(VecDoub_I &y, VecDoub_I &dydx, const Doub x, const Doub h, VecDoub_O &yout,
                     const Doub params[]);
    void euler(VecDoub_I &y, VecDoub_I &dydx, VecDoub_I &dW, const Doub x,
               const Doub h, VecDoub_O &yout);
    void IC_setup(VecDoub &y, Doub x0, Doub y0, Doub xdot0, Doub ydot0, Doub phi0,
                  Doub params[]);
    void diffusion(VecDoub_I & msd, const Int & samples, Doub & sigma);
    float gaussian_noise();
    void gen_gnuplot(const QString & name);
    void filter(QMap<double, double> & map, const Doub & eps);
    void bifparam(const int &v, const QString & param, const Doub &r, Doub params[]);
    QuPlot *c;

    QMultiMap<double, double> rkValues_x, rkValues_y,
                                rkValues_xdot, rkValues_ydot,
                                rkAvgvelo_x, rkAvgvelo_y, rkAvgvelo_phi,
                                rkValues_xtemp, rkValues_ytemp, rkValues_phitemp,
                                rkValues_p1x, rkValues_p2x,
                                rkValues_p1y, rkValues_p2y,
                                rkValues_phi, rkValues_phidot,
                                rkVariance_x, rkVariance_y, rkVariance_phi,
                                rkDist_x, rkDist_y, rkDist_phi,
                                rkDefAngle,
                                rkValues_vx, rkValues_vy, rkValues_vphi;

    QMultiMap<double, double> euValues_x, euValues_y,
                                euValues_xdot, euValues_ydot,
                                euAvgvelo_x, euAvgvelo_y, euAvgvelo_phi,
                                euValues_xtemp, euValues_ytemp, euValues_phitemp,
                                euValues_p1x, euValues_p2x,
                                euValues_p1y, euValues_p2y,
                                euValues_phi, euValues_phidot,
                                euVariance_x, euVariance_y, euVariance_phi,
                                euDist_x, euDist_y, euDist_phi,
                                euDefAngle,
                                euValues_vx, euValues_vy, euValues_vphi;


    QMultiMap<double, double> NullMap; // dummy var for use with write_to_file()

    QString file_rk_bif, file_rk_avg, file_rk_out, file_rk_pt1, file_rk_pt2,
    file_rk_biv, file_rk_conv, file_rk_variance, file_rk_distance,
    file_rk_defangle, file_rk_dualbif, file_rk_vel;
    QString file_eu_bif, file_eu_avg, file_eu_out, file_eu_pt1, file_eu_pt2,
    file_eu_biv, file_eu_conv, file_eu_variance, file_eu_distance,
    file_eu_defangle, file_eu_dualbif, file_eu_vel;

    Int v;
    Doub T;


};


//=============================================================================
#endif // CALCULATOR_H defined
//=============================================================================
