import math
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib.pyplot import *
from pylab import *

DATA_RK = "/home/raw/rk_conv.dat"
DATA_EU = "/home/raw/eu_conv.dat"
FOUT = "../converge.png"

xlabel = r'$h$'
ylabel = r'$\delta y$'
labelsize = 24
res = (1440/80,900/80) # default dpi is 80

######################################################################

def mean_var(param,var):
	unique_param = unique(param)
	list_var = [ [] for DUMMYVAR in range(len(unique_param)) ]
	ret_avg = []
	for i in range(len(unique_param)):
		print "now:", unique_param[i]
		for j in range(len(var)):
			if (param[j] == unique_param[i]):
				list_var[i].append(var[j])
				#assert len(list_var[i]) == len(param/unique_param)
		ret_avg.append(float(sum(list_var[i])) /
				len(list_var[i]))
		#print list_var[i], ret_avg[i]
		#assert list_var[i] == ret_avg[i]
	return ret_avg

print "reading data..."

rk = mlab.csv2rec(DATA_RK, delimiter='\t', comments='#')
eu = mlab.csv2rec(DATA_EU, delimiter='\t', comments='#')

h_rk = rk.r
h_eu = eu.r
x_rk = rk.x
x_eu = eu.x

h_min = h_rk.min()
h_max = h_rk.max()

print "calculating mean..."

rk_mean = mean_var(h_rk, x_rk)
eu_mean = mean_var(h_eu, x_eu)

#print rk_mean


fig = plt.figure() #(figsize=res)

#ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(111)

#ax1.plot(unique(h_rk), rk_mean, marker=',', ls='')
#ax1.plot(unique(h_eu), eu_mean, marker=',', ls='')

#ax1.axhline(x_rk[0], color='r')
#ax1.axhline(x_eu[0], color='r')

print "calculating error..."

err_rk = []
err_eu = []

for i in range(len(rk_mean)):
	tmp = np.abs(rk_mean[i] - rk_mean[0])
	err_rk.append(tmp)

for i in range(len(eu_mean)):
	tmp = np.abs(eu_mean[i] - rk_mean[0])
	err_eu.append(tmp)

print "plotting..."
ax2.loglog(unique(h_rk), err_rk, marker=',', ls='', ms=0.1)
ax2.loglog(unique(h_eu), err_eu, marker=',', ls='', ms=0.1)

pad = h_max

ax2.set_xlabel(xlabel, size=labelsize)
ax2.set_ylabel(ylabel, size=labelsize)
#ax2.set_xlim(h_min,h_max)
#ax2.set_ylim(min(err_eu),1)

#ax1.set_xlim()
#ax1.set_ylim(-3.3,-3.2)
plt.savefig(FOUT)
