#include "dimdialog.h"
#include "ui_dimdialog.h"
#include "calculator.h"
#include "qp3.h"
#include "ran.h"

DimDialog::DimDialog(QuPlot *parent) :
    QDialog(parent),
    m_ui(new Ui::DimDialog)
{
    m_ui->setupUi(this);
    qDebug() << "This is DimDialog";

    d = parent;
    pi = M_PI;
    srand(time(NULL));
    initDialog();
}

DimDialog::~DimDialog()
{
    delete m_ui;
}

void DimDialog::initDialog()
{
    c = m_ui->comboBox;
    from = m_ui->fromSpinBox;
    to = m_ui->toSpinBox;
    from->setEnabled(false);
    to->setEnabled(false);
    m_ui->comboBox->setCurrentIndex(3);
    m_ui->threshSpinBox->setEnabled(false);
    m_ui->searchCheck->setEnabled(false);
    m_ui->fromr1SpinBox->setEnabled(false);
    m_ui->fromr2SpinBox->setEnabled(false);
    m_ui->tor1SpinBox->setEnabled(false);
    m_ui->tor2SpinBox->setEnabled(false);
    m_ui->stepr1SpinBox->setEnabled(false);
    m_ui->stepr2SpinBox->setEnabled(false);
    m_ui->param1Edit->setEnabled(false);
    m_ui->param2Edit->setEnabled(false);
    m_ui->searchCheck->setEnabled(false);
    m_ui->dualbifCheck->setEnabled(false);
    list.append("david");
    list.append("damped harmonic oszillator");
    list.append("harmonic oszillator");
    list.append("rotating dimer");
    qDebug() << "inserting list into comboBox";
    c->insertItems(0,list);
    qDebug() << "# of items in comboBox: " << c->count();
    /*
    for (i=0;i<list.size();i++) {
        c->setItemText(i,list.at(i));
        qDebug() << i << ": " << c->itemText(i);
    }
    */
    c->setCurrentIndex(0);
}
/*
    QRegExp rx("^[\\d-\\D]+$");

    ui_f1Label = qFindChild<QLabel*>(this,"f1Label");
    ui_f0Line = qFindChild<QLineEdit*>(this,"f0Line");
    ui_f1Line = qFindChild<QLineEdit*>(this,"f1Line");

    ui_f0Validator = new QRegExpValidator(rx,ui_f0Line);
    ui_f1Validator = new QRegExpValidator(rx,ui_f1Line);

    ui_f0Line->setValidator(ui_f0Validator);
    ui_f1Line->setValidator(ui_f1Validator);

   // ui_f1Label->setEnabled(ui_f0Line->hasAcceptableInput() == true);
   // ui_f1Line->setEnabled(ui_f0Line->hasAcceptableInput() == true);
    */

void DimDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
void DimDialog::on_convergeCheck_stateChanged() {
    qDebug() << "converge check toggled";
     bfBox = m_ui->bifurcateCheck;
     from = m_ui->fromSpinBox;
     to = m_ui->toSpinBox;
     stepsize = m_ui->stepsizeSpinBox;
     steps = m_ui->stepsSpinBox;
     velox = m_ui->veloxSpinBox;
     veloy = m_ui->veloySpinBox;
     cmcheck = m_ui->convergeCheck->isChecked();
     if (!cmcheck) {
         d->valuesMap.insert(11,0);
         from->setEnabled(false);
         to->setEnabled(false);
         stepsize->setEnabled(true);
         bfBox->setEnabled(true);
         velox->setValue(100);
         veloy->setValue(100);
         steps->setValue(1000000);
     } else {
         qDebug() << "hello";
         d->valuesMap.insert(11,1);
         from->setEnabled(true);
         to->setEnabled(true);
         stepsize->setEnabled(false);
         bfBox->setEnabled(false);
         velox->setValue(0);
         veloy->setValue(0);
         steps->setValue(20);
     }
 }

void DimDialog::on_bifurcateCheck_stateChanged()
{
    bfcheck = m_ui->bifurcateCheck->isChecked();
    if (!bfcheck) {
        d->valuesMap.insert(11,0);
        m_ui->fromr1SpinBox->setEnabled(false);
        m_ui->fromr2SpinBox->setEnabled(false);
        m_ui->tor1SpinBox->setEnabled(false);
        m_ui->tor2SpinBox->setEnabled(false);
        m_ui->stepr1SpinBox->setEnabled(false);
        m_ui->stepr2SpinBox->setEnabled(false);
        m_ui->samplesSpinBox->setEnabled(false);
        m_ui->convergeCheck->setEnabled(true);
        c->setEnabled(true);
        m_ui->param1Edit->setEnabled(false);
        m_ui->param2Edit->setEnabled(false);
        m_ui->fromtSpinBox->setValue(0.0);
        m_ui->dualbifCheck->setEnabled(false);
    } else {
        d->valuesMap.insert(11,2);
        m_ui->fromr1SpinBox->setEnabled(true);
        m_ui->tor1SpinBox->setEnabled(true);
        m_ui->stepr1SpinBox->setEnabled(true);
        m_ui->samplesSpinBox->setEnabled(true);
        m_ui->convergeCheck->setEnabled(false);
        c->setEnabled(false);
        m_ui->param1Edit->setEnabled(true);
        m_ui->fromtSpinBox->setValue(0.9);
        m_ui->dualbifCheck->setEnabled(true);
    }
}

void DimDialog::on_dualbifCheck_stateChanged()
{
    bf2check = m_ui->dualbifCheck->isChecked();
    if (!bf2check) {
        d->valuesMap.insert(11,2);
        m_ui->fromr2SpinBox->setEnabled(false);
        m_ui->tor2SpinBox->setEnabled(false);
        m_ui->stepr2SpinBox->setEnabled(false);
        m_ui->param2Edit->setEnabled(false);
    }
    else {
        d->valuesMap.insert(11,3);
        m_ui->fromr2SpinBox->setEnabled(true);
        m_ui->tor2SpinBox->setEnabled(true);
        m_ui->stepr2SpinBox->setEnabled(true);
        m_ui->param2Edit->setEnabled(true);
    }
}

double DimDialog::randpi(Ran &r)

{
    Doub s = 2.0*M_PI*r.doub();
    return s;
}

void DimDialog::on_randomCheck_stateChanged()
{
    Ran ran(time(NULL));
    randcheck = m_ui->randomCheck->isChecked();
    if (!randcheck)
    {
        d->valuesMap.insert(28,0);
        m_ui->offxSpinBox->setEnabled(true);
        m_ui->offySpinBox->setEnabled(true);
        m_ui->veloxSpinBox->setEnabled(true);
        m_ui->veloySpinBox->setEnabled(true);
        m_ui->phiSpinBox->setEnabled(true);
    } else {
        d->valuesMap.insert(28,1);
        m_ui->offxSpinBox->setValue(randpi(ran));
        m_ui->offySpinBox->setValue(randpi(ran));
        m_ui->veloxSpinBox->setValue(randpi(ran));
        m_ui->veloySpinBox->setValue(randpi(ran));
        m_ui->phiSpinBox->setValue(randpi(ran)*180/PI);
        m_ui->offxSpinBox->setEnabled(false);
        m_ui->offySpinBox->setEnabled(false);
        m_ui->veloxSpinBox->setEnabled(false);
        m_ui->veloySpinBox->setEnabled(false);
        m_ui->phiSpinBox->setEnabled(false);

    }
}

void DimDialog::on_searchCheck_stateChanged()
{
    searchcheck = m_ui->searchCheck->isChecked();
    if (!searchcheck) {
        m_ui->offxSpinBox->setEnabled(true);
        m_ui->offySpinBox->setEnabled(true);
        m_ui->veloxSpinBox->setEnabled(true);
        m_ui->veloySpinBox->setEnabled(true);
        m_ui->threshSpinBox->setEnabled(false);
    } else {
        m_ui->offxSpinBox->setEnabled(false);
        m_ui->offySpinBox->setEnabled(false);
        m_ui->veloxSpinBox->setEnabled(false);
        m_ui->veloySpinBox->setEnabled(false);
        m_ui->threshSpinBox->setEnabled(true);
    }
}

void DimDialog::on_comboBox_currentIndexChanged()
{
    int index = c->currentIndex();

    switch (index) {
        case 0:
        m_ui->offxSpinBox->setValue(1.0);
        m_ui->veloxSpinBox->setValue(100.0);
        m_ui->eta1SpinBox->setValue(0.465);
        m_ui->eta2SpinBox->setValue(0.0);
        m_ui->FSpinBox->setValue(0.1);
        m_ui->wSpinBox->setValue(0.6);
        m_ui->fromr1SpinBox->setValue(0.45);
        m_ui->tor1SpinBox->setValue(0.55);
        m_ui->stepr1SpinBox->setValue(0.0001);
        m_ui->samplesSpinBox->setValue(3);
        m_ui->wSpinBox->setValue(0.6);
        m_ui->aSpinBox->setValue(1.0);
        m_ui->bSpinBox->setValue(1.24);
        m_ui->param1Edit->setText("E");
        break;
        case 3:
        m_ui->offxSpinBox->setValue(0);
        m_ui->offySpinBox->setValue(0);
        m_ui->veloxSpinBox->setValue(0);
        m_ui->veloySpinBox->setValue(0);
        m_ui->phiSpinBox->setValue(0);
        m_ui->eta1SpinBox->setValue(1.5);
        m_ui->eta2SpinBox->setValue(1.0);
        m_ui->FSpinBox->setValue(0.00);
        m_ui->wSpinBox->setValue(0.1);
        m_ui->fromr1SpinBox->setValue(0.0);
        m_ui->tor1SpinBox->setValue(180);
        m_ui->stepr1SpinBox->setValue(1);
        m_ui->samplesSpinBox->setValue(3);
        m_ui->fromtSpinBox->setValue(0);
        m_ui->tempSpinBox->setValue(0.0);
        m_ui->aSpinBox->setValue(1.15);
        m_ui->bSpinBox->setValue(58.0);
        m_ui->lSpinBox->setValue(2.5);
        m_ui->thetaSpinBox->setValue(0.0);
        m_ui->psiSpinBox->setValue(0.0);
        m_ui->param1Edit->setText("A");
        m_ui->param2Edit->setText("l");
        m_ui->fromr1SpinBox->setValue(0.0);
        m_ui->tor1SpinBox->setValue(90.0);
        m_ui->fromr2SpinBox->setValue(1.0);
        m_ui->tor2SpinBox->setValue(10.0);
        m_ui->USpinBox->setValue(1.0);
        break;
    }
}

void DimDialog::on_pushButton_clicked()
{
    int index = c->currentIndex();

    qDebug() << "ui_comboBox->currentIndex() = " << index;
    qDebug() << "populating QMap";

    d->valuesMap.insert(7,m_ui->timeSpinBox->value());
    d->valuesMap.insert(8,m_ui->offxSpinBox->value());
    d->valuesMap.insert(23,m_ui->offySpinBox->value());
    d->valuesMap.insert(9,m_ui->unusedSpinBox->value());
    d->valuesMap.insert(10,m_ui->veloxSpinBox->value());
    d->valuesMap.insert(24,m_ui->veloySpinBox->value());
    d->valuesMap.insert(22,m_ui->fromtSpinBox->value());
    d->valuesMap.insert(25,m_ui->eta1SpinBox->value());
    d->valuesMap.insert(26,m_ui->eta2SpinBox->value());
    d->valuesMap.insert(2,m_ui->aSpinBox->value());
    d->valuesMap.insert(3,m_ui->bSpinBox->value());
    d->valuesMap.insert(4,m_ui->FSpinBox->value());
    d->valuesMap.insert(6,m_ui->wSpinBox->value());
    d->valuesMap.insert(27,m_ui->phiSpinBox->value());
    d->valuesMap.insert(32,m_ui->tempSpinBox->value());
    d->valuesMap.insert(5,m_ui->lSpinBox->value());
    d->valuesMap.insert(35,m_ui->samplesSpinBox->value());
    d->valuesMap.insert(36,m_ui->filterSpinBox->value());
    d->valuesMap.insert(13,m_ui->stepsizeSpinBox->value());
    d->valuesMap.insert(14,m_ui->stepsSpinBox->value());
    d->valuesMap.insert(40,m_ui->thetaSpinBox->value());
    d->valuesMap.insert(41,m_ui->psiSpinBox->value());
    d->valuesMap.insert(42,m_ui->USpinBox->value());
    d->valuesMap.insert(43,m_ui->hstepSpinBox->value());

    d->valuesMap.insert(29,0); /* disable euler for now */

    switch (index) {
        case 0:
        d->valuesMap.insert(0,2);
        d->valuesMap.insert(1,1);
        break;
        case 1:
        d->valuesMap.insert(0,2);
        d->valuesMap.insert(1,2);
        break;
        case 2:
        d->valuesMap.insert(0,2);
        d->valuesMap.insert(1,3);
        break;
        case 3:
        d->valuesMap.insert(0,3);
        d->valuesMap.insert(1,4);
        break;
    }

    if (cmcheck) {
        d->valuesMap.insert(14,m_ui->stepsSpinBox->value());
        d->valuesMap.insert(15,m_ui->fromSpinBox->value());
        d->valuesMap.insert(16,m_ui->toSpinBox->value());
    }

    if (bfcheck) {
        d->valuesMap.insert(17,m_ui->fromr1SpinBox->value());
        d->valuesMap.insert(18,m_ui->tor1SpinBox->value());
        d->valuesMap.insert(19,m_ui->stepr1SpinBox->value());
        d->str1 = m_ui->param1Edit->text();
    }

    if (bf2check) {
        d->valuesMap.insert(37,m_ui->fromr2SpinBox->value());
        d->valuesMap.insert(38,m_ui->tor2SpinBox->value());
        d->valuesMap.insert(39,m_ui->stepr2SpinBox->value());
        d->str2 = m_ui->param2Edit->text();
    }

    if (!searchcheck) {
        d->valuesMap.insert(30,0);
    } else {
        d->valuesMap.insert(30,1);
        d->valuesMap.insert(31,m_ui->threshSpinBox->value());
    }

    d->eul = m_ui->eulCheck->isChecked();
    d->ruk = m_ui->rukCheck->isChecked();

    qDebug() << "Following values have been set:";
    QMapIterator<int,double> i(d->valuesMap);
    while (i.hasNext()) {
        i.next();
        qDebug() << i.key() << ": " << i.value();
    }
    qDebug() << "See quplot.h for an explanation";
}
