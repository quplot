import matplotlib as mpl
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.axes as axe
import matplotlib.collections as col
import numpy as np
import random
from pylab import *
from matplotlib.pyplot import *

FIN = '../data/dual_bif_alpha_l_55_65_1_4_f001.dat'
FOUT = '../db10.png'

samples = 10
ticksize = 14
labelsize = 24
title = "a = 1.15, w = 0.1, F = 0.00, eta1 = 1.0, eta2 = 1.5, theta = 0"
xlabel = r'$\alpha$'
ylabel = r'$l$'

xtfreq = 3
ytfreq = 1
xpad = 0.01
ypad = 0.01
ms = 80
ystep = 0.1
xstep = 0.1
res = (1024/80,768/80) # default dpi is 80
method = 'direct' # mean, direct
has_y = False
has_col = False
has_legend = False

################################################################################

def assign_label(color):
	ret_label = 'DUMMY'
	if (color == 'grey'):
		ret_label = r'$|v| = 0$'
	if (color == 'blue'):
		ret_label = r'$|v| = -1$'
	if (color == 'red'):
		ret_label = r'$|v| = 1$'
	if (color == 'orange'):
		ret_label = r'$|v| = 0.5$'
	if (color == 'green'):
		ret_label = r'$|v| = -0.5$'
	if (color == 'black'):
		ret_label = r'other rational $v$'
	if (color == 'cyan'):
		ret_label = r'$v < 0$'
	if (color == 'magenta'):
		ret_label = r'$v > 0$'
	return (ret_label)

def assign_color2(colors,xar,yar,vel,clist):
	print "---------------------------------------------------------"
	unique_colors = unique(colors)
	#print range(len(clist))
	ret_xcolors = [ [] for DUMMYVAR in range(len(clist)) ]
	ret_ycolors = [ [] for DUMMYVAR in range(len(clist)) ]
	i = 0
	assert len(xar) == len(yar) == len(colors)
	xarlen_trunc = len(xar)
	while i < xarlen_trunc:
		tmp = []
		tmpv = []
		for j in range(samples):
			assert i+j <= len(xar)
			#assert yar[i+j] == yar[i] and xar[i+j] == xar[i]
			tmp.append(colors[i+j])
			tmpv.append(vel[i+j])
#		print len(tmp), samples
		assert len(tmp) == samples
		ccount = len(unique(tmp))
		if ccount >= 2 and 'green' in tmp and 'orange' in tmp:
			ret_xcolors[6].append(xar[i])
			ret_ycolors[6].append(yar[i])
		elif ccount >= 2 and 'blue' in tmp and 'red' in tmp:
			ret_xcolors[7].append(xar[i])
			ret_ycolors[7].append(yar[i])
		elif ccount == 2 and 'blue' in tmp and 'black' in tmp:
			if tmp.count('blue') >= samples/samples:
				ret_xcolors[2].append(xar[i])
				ret_ycolors[2].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 2 and 'red' in tmp and 'black' in tmp:
			if tmp.count('red') >= samples/samples:
				ret_xcolors[5].append(xar[i])
				ret_ycolors[5].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 2 and 'green' in tmp and 'black' in tmp:
			if tmp.count('green') >= samples/samples:
				ret_xcolors[3].append(xar[i])
				ret_ycolors[3].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 2 and 'orange' in tmp and 'black' in tmp:
			if tmp.count('orange') >= samples/samples:
				ret_xcolors[4].append(xar[i])
				ret_ycolors[4].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 2 and 'blue' in tmp and 'grey' in tmp:
			if tmp.count('blue') >= samples/samples:
				ret_xcolors[2].append(xar[i])
				ret_ycolors[2].append(yar[i])
			else:
				ret_xcolors[1].append(xar[i])
				ret_ycolors[1].append(yar[i])
		elif ccount == 2 and 'red' in tmp and 'grey' in tmp:
			if tmp.count('red') >= samples/samples:
				ret_xcolors[5].append(xar[i])
				ret_ycolors[5].append(yar[i])
			else:
				ret_xcolors[1].append(xar[i])
				ret_ycolors[1].append(yar[i])
		elif ccount == 2 and 'green' in tmp and 'grey' in tmp:
			if tmp.count('green') >= samples/samples:
				ret_xcolors[3].append(xar[i])
				ret_ycolors[3].append(yar[i])
			else:
				ret_xcolors[1].append(xar[i])
				ret_ycolors[1].append(yar[i])
		elif ccount == 2 and 'orange' in tmp and 'grey' in tmp:
			if tmp.count('orange') >= samples/samples:
				ret_xcolors[4].append(xar[i])
				ret_ycolors[4].append(yar[i])
			else:
				ret_xcolors[1].append(xar[i])
				ret_ycolors[1].append(yar[i])
		elif ccount == 2 and 'grey' in tmp and 'black' in tmp:
			if tmp.count('grey') >= samples/2:
				ret_xcolors[1].append(xar[i])
				ret_ycolors[1].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])

		elif ccount == 2 and 'blue' in tmp and ('orange' in tmp or 'green' in tmp):
			if tmp.count('blue') >= samples/2:
				ret_xcolors[1].append(xar[i])
				ret_ycolors[1].append(yar[i])
			elif tmp.count('green') >= samples/2:
				ret_xcolors[3].append(xar[i])
				ret_ycolors[3].append(yar[i])
			elif tmp.count('orange') >= samples/2:
				ret_xcolors[4].append(xar[i])
				ret_ycolors[4].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 2 and 'red' in tmp and ('orange' in tmp or 'green' in tmp):
			if tmp.count('red') >= samples/2:
				ret_xcolors[5].append(xar[i])
				ret_ycolors[5].append(yar[i])
			elif tmp.count('green') >= samples/2:
				ret_xcolors[3].append(xar[i])
				ret_ycolors[3].append(yar[i])
			elif tmp.count('orange') >= samples/2:
				ret_xcolors[4].append(xar[i])
				ret_ycolors[4].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])



		elif ccount == 3 and 'green' in tmp and 'orange' in tmp and 'grey':
			ret_xcolors[6].append(xar[i])
			ret_ycolors[6].append(yar[i])
		elif ccount == 3 and 'blue' in tmp and 'red' in tmp and 'grey':
			ret_xcolors[7].append(xar[i])
			ret_ycolors[7].append(yar[i])

		elif ccount == 3 and 'blue' in tmp and ('grey' in tmp or 'black' in tmp):
			if tmp.count('blue') >= samples/2:
				ret_xcolors[6].append(xar[i])
				ret_ycolors[6].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 3 and 'red' in tmp and ('grey' in tmp or 'black' in tmp):
			if tmp.count('red') >= samples/2:
				ret_xcolors[6].append(xar[i])
				ret_ycolors[6].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 3 and 'green' in tmp and ('grey' in tmp or 'black' in tmp):
			if tmp.count('green') >= samples/2:
				ret_xcolors[7].append(xar[i])
				ret_ycolors[7].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 3 and 'orange' in tmp and ('grey' in tmp or 'black' in tmp):
			if tmp.count('orange') >= samples/2:
				ret_xcolors[7].append(xar[i])
				ret_ycolors[7].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])

		elif ccount == 4 and 'blue' and ('green' in tmp  or 'orange') in tmp and 'grey' and 'black' in tmp:
			if tmp.count('blue') >= samples/2:
				ret_xcolors[2].append(xar[i])
				ret_ycolors[2].append(yar[i])
			elif tmp.count('green') >= samples/2:
				ret_xcolors[3].append(xar[i])
				ret_ycolors[3].append(yar[i])
			elif tmp.count('orange') >= samples/2:
				ret_xcolors[4].append(xar[i])
				ret_ycolors[4].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 4 and 'red' and ('green' in tmp or 'orange' in tmp) and 'grey' and 'black' in tmp:
			if tmp.count('red') >= samples/2:
				ret_xcolors[5].append(xar[i])
				ret_ycolors[5].append(yar[i])
			elif tmp.count('green') >= samples/2:
				ret_xcolors[3].append(xar[i])
				ret_ycolors[3].append(yar[i])
			elif tmp.count('orange') >= samples/2:
				ret_xcolors[4].append(xar[i])
				ret_ycolors[4].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 4 and 'green' in tmp and 'grey' and 'black' in tmp:
			if tmp.count('green') >= samples/2:
				ret_xcolors[3].append(xar[i])
				ret_ycolors[3].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])
		elif ccount == 4 and 'orange' in tmp and 'grey' and 'black' in tmp:
			if tmp.count('orange') >= samples/2:
				ret_xcolors[4].append(xar[i])
				ret_ycolors[4].append(yar[i])
			else:
				ret_xcolors[0].append(xar[i])
				ret_ycolors[0].append(yar[i])

		elif ccount == 4 and 'blue' in tmp and 'red' in tmp and 'green' in tmp and 'orange' in tmp:
			print tmp
			ret_xcolors[11].append(xar[i])
			ret_ycolors[11].append(yar[i])
		elif ccount == 4 and 'green' in tmp and 'orange' in tmp and 'grey' in tmp and 'black' in tmp:
			ret_xcolors[6].append(xar[i])
			ret_ycolors[6].append(yar[i])
		elif ccount == 4 and 'blue' in tmp and 'red' in tmp and 'grey' in tmp and 'black' in tmp:
			ret_xcolors[7].append(xar[i])
			ret_ycolors[7].append(yar[i])
		elif ccount == 1 and 'grey' in tmp:
			ret_xcolors[1].append(xar[i])
			ret_ycolors[1].append(yar[i])
		elif ccount == 1 and 'blue' in tmp:
			ret_xcolors[2].append(xar[i])
			ret_ycolors[2].append(yar[i])
		elif ccount == 1 and 'green' in tmp:
			ret_xcolors[3].append(xar[i])
			ret_ycolors[3].append(yar[i])
		elif ccount == 1 and 'orange' in tmp:
			ret_xcolors[4].append(xar[i])
			ret_ycolors[4].append(yar[i])
		elif ccount == 1 and 'red' in tmp:
			ret_xcolors[5].append(xar[i])
			ret_ycolors[5].append(yar[i])
		elif len(unique(tmpv)) >= samples:
			#print unique(tmpv)
			ret_xcolors[0].append(xar[i])
			ret_ycolors[0].append(yar[i])
		elif ccount == 1 and 'black' in tmp:
			ret_xcolors[0].append(xar[i])
			ret_ycolors[0].append(yar[i])
		else:
			print unique(tmp)
			#print unique(tmpv)
			ret_xcolors[0].append(xar[i])
			ret_ycolors[0].append(yar[i])
		i = i+samples
		#print "increment:",i
	for j  in range(len(clist)):
		print clist[j],":",len(ret_xcolors[j])
	#print "unmatched velocities:"
	#print ret_xcolors[13], ret_ycolors[13]
        print "---------------------------------------------------------"
        return (ret_xcolors, ret_ycolors)

def assign_color(colors,xar,yar):
	print "---------------------------------------------------------"
	unique_colors = unique(colors)
	ret_xcolors = [ [] for DUMMYVAR in range(len(unique_colors)) ]
	ret_ycolors = [ [] for DUMMYVAR in range(len(unique_colors)) ]
	for j in range(len(unique_colors)):
		tmpx = []
		tmpy = []
		k = 0
		for i in range(len(colors)):
			if (colors[i] == unique_colors[j]):
				k = k+1
				tmpxcolors = ret_xcolors[j]
				tmpycolors = ret_ycolors[j]
				#print tmpxcolors
				if len(tmpxcolors) > 0:
					if (tmpxcolors[-1] != xar[i]) or (tmpycolors[-1] != yar[i]):
						#print tmpxcolors[-1],tmpycolors[-1],"is not", xar[i],yar[i],"!"
						ret_xcolors[j].append(xar[i])
						ret_ycolors[j].append(yar[i])
				else: 
					ret_xcolors[j].append(xar[i])
					ret_ycolors[j].append(yar[i])
		lenc = float(len(colors))
		per = 100*k/lenc
		print unique_colors[j],":",len(ret_xcolors[j]),k,"(",'%.2f' % per ,"%)"
	print "---------------------------------------------------------"
	return (ret_xcolors, ret_ycolors)

print "reading data..."
r  = mlab.csv2rec(FIN, delimiter='\t', comments='#')

x = r.x
x_color = r.vx_color
y_color = r.vy_color
y = r.y
vx = r.vx
vy = r.vy

assert len(x) == len(y) == len(vx) == len(vy)
print "done."

xmin = int(np.min(unique(x)))
xmax = int(np.max(unique(x)))
ymin = int(np.min(unique(y)))
ymax = int(np.max(unique(y)))

# assumes that the step distance doesn't change over the course of time
rat = xstep/ystep
print "size of xstep:", xstep
print "size of ystep:", ystep
print "aspect ratio:",rat

print "different values of vx:"
print unique(vx),"(",len(vx),"total)"

print "different values of vy:"
print unique(vy),"(",len(vy),"total)"

# needed for assign_color2
color_list = [
		'black',	# 0	everything else
		'gray',		# 1	v = 0
		'blue',		# 2	v = -1.0
		'green',	# 3	v = -0.5
		'orange',	# 4	v = 0.5
		'red',		# 5	v = 1.0
		'khaki',	# 6	v = -0.5 && v = 0.5
		'purple',	# 7	v = -1.0 && v = 1.0
		'white',	# 8	not understood
		'darkgoldenrod',# 9	v = +/- 0.5 & 0
		'mediumpurple',	# 10	v = +/- 1 1 & -0
		'brown',	# 11	v = +/1 1 & +/- 0.5
	]

label_list = [
		r'other rational $v$',	# 0 black
		r'$v = 0$',			# 1 grey
		r'$v = -1$',			# 2 blue
		r'$v = -\frac{1}{2}$',		# 3 green
		r'$v = \frac{1}{2}$',		# 4 orange
		r'$v = 1$',			# 5 red
		r'$v = \pm \frac{1}{2}$',	# 6 brown
		r'$v = \pm 1$',			# 7 purple
		r'???',				# 8 white
		r'$v = \pm \frac{1}{2},$ other',# 9 darkgoldenrod
		r'$v = \pm 1,$ other ',		# 10 mediumpurple
		r'$v = \pm 1,\pm \frac{1}{2}$',	# 11 brown
	]

xpadmin = xmin-xstep
xpadmax = xmax+xstep
ypadmin = ymin-ystep
ypadmax = ymax+ystep
xlim = (xpadmin, xpadmax)		# small padding between plot and
ylim = (ypadmin, ypadmax)		# axis so they don't overlap
xticks = np.arange(xmin, xmax+xpad, xtfreq)
yticks = np.arange(ymin, ymax+ypad, ytfreq)


fig = plt.figure(figsize=res)
#fig.suptitle(title, size=labelsize)

max = 0.84
c = max/2
rect1 = [0.1, 0.1, c, 0.9]
rect2 = [c+0.125, 0.1, c, 0.9]
rect = [0.1, 0.1, max, 0.9]

if has_y is True:
	#ax1 = fig.add_subplot(121, aspect=rat)
	#ax2 = fig.add_subplot(122, aspect=rat)
	ax1 = fig.add_axes(rect1, aspect=rat)
	ax2 = fig.add_axes(rect2, aspect=rat)
else:
	#ax1 = fig.add_subplot(111, aspect=rat)
	ax1 = fig.add_axes(rect, aspect=rat)


if (method == 'mean'):

	print "assigning colors...."
	xcolors_x,xcolors_y = assign_color(x_color,x,y)
	ycolors_x,ycolors_y = assign_color(y_color,x,y)
	print "done."
	
	print "plotting..."
	for k in range(len(unique(x_color))):
		x_xtli = xcolors_x[k]
		x_ytli = xcolors_y[k]
		labelx = assign_label(unique(x_color)[k])
		ax1.scatter(x_xtli, x_ytli, marker='s', lw=0, s=ms, alpha=0.5,
				c = unique(x_color)[k], label=labelx,
				edgecolor='face')
	if (has_y is True):
		for k in range(len(unique(y_color))):
			y_xtli = ycolors_x[k]
			y_ytli = ycolors_y[k]
			labely = assign_label(unique(y_color)[k])
			ax2.scatter(y_xtli, y_ytli, marker='s', lw=0,
					s=ms, alpha=0.5, c =
					unique(y_color)[k],
					label=labely, edgecolor='none')

	print "done."
	
elif (method == 'direct'):

	print "assigning colors..."
	xcolors_x,xcolors_y = assign_color2(x_color,x,y,vx,color_list)
	ycolors_x,ycolors_y = assign_color2(y_color,x,y,vy,color_list)
	print "done."

	#for i in range(8):
	#	print i, xcolors_x[i],xcolors_y[i];
	
	print "plotting..."
	for k in range(len(xcolors_x)):
		if len(xcolors_x[k]) > 0:
			x_xtli = xcolors_x[k]
			x_ytli = xcolors_y[k]
			ax1.scatter(x_xtli, x_ytli, marker='s', lw=0,

					s=ms, alpha=1, c=color_list[k],
					label=label_list[k])

	if (has_y is True):
		for k in range(len(ycolors_x)):
			if len(ycolors_x[k]) > 0:
				y_xtli = ycolors_x[k]
				y_ytli = ycolors_y[k]
				ax2.scatter(y_xtli, y_ytli, marker='s',
						lw=0, s=ms, alpha=1,
						c=color_list[k],
						label=label_list[k])

	print "done."
else:
	print "fatal error: specify a valid method"

if has_legend is True:
	leg1 = ax1.legend(loc=0, shadow=True,
			fancybox=True, scatterpoints=1, markerscale=1,
			borderaxespad=0.)
	for t in leg1.get_texts():
		t.set_fontsize('small')
	leg1.get_frame().set_alpha(0.75)
	if has_y is True:
		leg2 = ax2.legend(loc=2, shadow=True,
				fancybox=True, scatterpoints=1, markerscale=1,
				borderaxespad=0.)
		for t in leg2.get_texts():
			t.set_fontsize('small')
		leg2.get_frame().set_alpha(0.75)



ax1.set_title(r'$v_x$', size=labelsize)
ax1.set_xlim(xlim)
ax1.set_ylim(ylim)
ax1.set_xlabel(xlabel, size=labelsize)
ax1.set_ylabel(ylabel, size=labelsize)
ax1.set_xticks(xticks, minor=False)
ax1.set_yticks(yticks, minor=False)
ax1.set_xticklabels(ax1.get_xticks(), size=ticksize)
ax1.set_yticklabels(ax1.get_yticks(), size=ticksize)

if has_y is True:
	ax2.set_title(r'$v_y$', size=labelsize)
	ax2.set_xlim(xlim)
	ax2.set_ylim(ylim)
	ax2.set_xlabel(xlabel, size=labelsize)
	ax2.set_ylabel('', size=labelsize)
	ax2.set_xticks(xticks, minor=False)
	ax2.set_yticks(yticks, minor=False)
	ax2.set_xticklabels(ax2.get_xticks(), size=ticksize)
	ax2.set_yticklabels('', size=ticksize)

if (has_col is True):
	s1 = np.arange(54.5,66,0.5)
	collection = col.BrokenBarHCollection.span_where(s1, ymin=0.95, ymax=4.05,
		where=s1>54, facecolor='None', edgecolor='red')
	ax1.add_collection(collection)


#plt.subplots_adjust(hspace=0)
print "flushing into a file..."
plt.savefig(FOUT)
print "done."
#plt.show()




