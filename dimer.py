#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import matplotlib as mpl
mpl.rc('text', usetex = True)
import pylab
import numpy as np
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.colors as col

FOUT = "../latex/images/dimer_intro.png"

L = 2*np.pi
w = 0.1
tau = L/w
angle1 = 30
angle2 = 60
transport_angle = 100
phi = 0
alpha = 135
dimer_length = 0.4
cofx = cofy = L/2
pad = 0.01 # small padding so the ticks get drawn correctly...
msize = 20
hw = 0.15 # arrow head width
res = (800/80,600/80) # default dpi is 80
cmap = cm.gray_r # colormap
shader = False # applies ultra cool shadows to the map!
show_plot = False
xticks = [-2*L,-L,0,L,2*L]
yticks = [-2*L,-L,0,L,2*L]
tlabel = ["-2L","-L","0","L","2L"]
xlabel = r'$x$'
ylabel = r'$y$'
cblabel = r'$\cos{x}\cos{y}+\cos{x}+\cos{y}$'
fs_ticks = 18
fs_labels = 28

################################################################################

PI = np.pi
rad1 = angle1*PI/180
rad2 = angle2*PI/180
rad_transport = transport_angle*PI/180
rad_phi = phi*PI/180
rad_alpha = alpha*PI/180

limx_min = -L/2
limx_max = 1.5*L
# quadratic
limy_min = limx_min
limy_max = limx_max

print "generating basemap..."
delta = 0.01
x = np.arange(limx_min,limx_max, delta)
y = np.arange(limy_min,limy_max, delta)
X,Y = np.meshgrid(x, y)
Z = np.cos(X)*np.cos(Y)+np.cos(X)+np.cos(Y)
print "done."

fig = plt.figure(figsize=res)

ax = fig.add_subplot(111)

if (shader is True):
    print "applying lightsource..."
    ls = col.LightSource(azdeg=0,altdeg=90)
    rgb = ls.shade(Z,cmap)
    print "done."
    print "plotting basemap..."
    pot = plt.imshow(rgb, extent=[limx_min,limx_max,limy_min,limy_max])
    print "done."
else:
    print "plotting basemap..."
    pot = plt.pcolormesh(X,Y,Z, cmap=cmap)
    print "done."
    
cb = plt.colorbar(pot)
cb.set_label(cblabel, size=fs_labels)

print "plotting dimer onto the basemap..."

def make_circle(start,stop,r):
	t = np.arange(start,stop,0.01)
	#t = t.reshape((len(t), 1))
	ret_x = cofx + r * np.cos(t)
	ret_y = cofy + r * np.sin(t)
	return ret_x, ret_y


varphi_plot_x, varphi_plot_y = make_circle(rad1, rad2, 2.0)
psi_plot_x, psi_plot_y = make_circle(rad_phi, rad_transport, 0.8)

ax.scatter(cofx, cofy, c='yellow', marker='o', lw=0, s=msize*4,
label='cof')
ax.arrow(cofx, cofy, np.cos(rad1)*dimer_length*L,
		np.sin(rad1)*dimer_length*L, color='r', lw=0.1, alpha=0.5)
ax.arrow(cofx, cofy, np.cos(rad2)*dimer_length*L,
		np.sin(rad2)*dimer_length*L, color='r', lw=0.1, alpha=0.5)
ax.arrow(cofx, cofy, -np.cos(rad1)*dimer_length*L,
		-np.sin(rad1)*dimer_length*L, color='r', lw=0.1, alpha=0.5)
ax.arrow(cofx, cofy, -np.cos(rad2)*dimer_length*L,
		-np.sin(rad2)*dimer_length*L, color='r', lw=0.1, alpha=0.5)
ax.scatter(cofx+np.cos(rad1)*dimer_length*L,
		cofy+np.sin(rad1)*dimer_length*L, c='blue', marker='o',
		lw=0, s=msize, label='x1')
ax.scatter(cofx-np.cos(rad1)*dimer_length*L,
		cofy-np.sin(rad1)*dimer_length*L, c='purple', marker='o',
		lw=0, s=msize, label='x1')
ax.scatter(cofx+np.cos(rad2)*dimer_length*L,
		cofy+np.sin(rad2)*dimer_length*L, c='blue',
		marker='o', lw=0, s=msize, label='x2')
ax.scatter(cofx-np.cos(rad2)*dimer_length*L,
		cofy-np.sin(rad2)*dimer_length*L, c='purple',
		marker='o', lw=0, s=msize, label='x2')
ax.plot(varphi_plot_x, varphi_plot_y, color='black')
ax.arrow(cofx, cofy, np.cos(rad_phi)*0.4*L, np.sin(rad_phi)*0.4*L,
		head_width=hw, shape='full', color='red')
ax.arrow(cofx, cofy, np.cos(rad_transport)*0.4*L,
		np.sin(rad_transport)*0.4*L, head_width=hw,
		shape='full')
ax.plot(psi_plot_x, psi_plot_y, color='black')
ax.arrow(cofx, cofy, np.cos(rad_alpha)*0.4*L, np.sin(rad_alpha)*0.4*L,
		head_width=hw, shape='full', color='blue')
ax.arrow(cofx, cofy, -np.cos(rad_alpha)*0.4*L, -np.sin(rad_alpha)*0.4*L,
		head_width=hw, shape='full', color='blue')

plt.text(cofx+np.cos(rad_phi+0.025)*0.4*L,
		cofy+np.sin(rad_phi+0.025)*0.4*L,
		r'$F\vec{e}_{\phi}$',{'color' : 'k', 'fontsize' :
			fs_labels})
plt.text(cofx+np.cos(rad_transport-0.05)*0.4*L,
		cofy+np.sin(rad_transport-0.05)*0.4*L, r'$\vec{v}$',
		{'color': 'k', 'fontsize' : fs_labels})
plt.text(0.5*L,0.65*L, r'$\psi$', {'color' : 'k', 'fontsize' : fs_labels})
plt.text(0.65*L,0.65*L, r'$\varphi$', {'color' : 'k', 'fontsize' :
	fs_labels})
plt.text(cofx+np.cos(rad_alpha-0.025)*0.4*L, cofy+np.sin(rad_alpha-0.025)*0.4*L,
		r'$A\vec{e}_{\alpha}$', {'color' : 'k', 'fontsize' :
			fs_labels})

ax.set_xlabel(xlabel, size=fs_labels)
ax.set_ylabel(ylabel, size=fs_labels)
ax.set_xticks(xticks)
ax.set_yticks(yticks)
ax.set_xlim(limx_min,limx_max)
ax.set_ylim(limy_min,limy_max)
ax.set_xticklabels(tlabel, size=fs_ticks)
ax.set_yticklabels(tlabel, size=fs_ticks)


print "saving to file.."
plt.savefig(FOUT)
print "done."

if (show_plot is True):
	print "showing plot..."
	plt.show()
	print "done."

