# -------------------------------------------------
# Project created by QtCreator 2009-05-16T01:52:03
# -------------------------------------------------
QT += opengl
LIBS += -lm
TARGET = QuPlot
TEMPLATE = app
SOURCES += main.cpp \
    quplot.cpp \
    dimdialog.cpp \
    calculator.cpp
HEADERS += quplot.h \
    dimdialog.h \
    calculator.h \
    qp3.h \
    ran.h \
    deviates.h
FORMS += quplot.ui \
    dimdialog.ui
OTHER_FILES += README.txt
