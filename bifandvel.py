# import stuff
import math
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.axes as axe
import matplotlib.text as txt

DATAX = '../data/rk_bifurcate_l_f001.dat'
DATAV = '../data/rk_bif_avg_velo_l_f001.dat'

DATAEU = []
DATAEU.append("/home/raw/eu_bif_avg_velo_t5.dat")
DATAEU.append("/home/raw/eu_bif_avg_velo_t4.dat")
DATAEU.append("/home/raw/eu_bif_avg_velo_t3.dat")

FOUT = '../latex2/images/bifurcation_l_f001.png'

# some macros
PI = math.pi
L = 2*PI

# options
xpad = 0.000002
ypad = 0.1
max = 0.875
tsize = 34
ticksize = 26
has_temp = False

# default values
xtks1 = []
ytks1 = [0,L/2,L]
ytks2 = [-1,-0.5,0,0.5,1]
xtkl1 = []
ytkl1 = ["0","L/2","L"]
ylim1 = [0-ypad,L+ypad]
#ylim2 = [-1-ypad,1+ypad]
xlbl1 = r''
xlbl2 = r'$l$'
ylbl1 = r'$x(k\tau)$ mod $L$'
ylbl2 = r'$v_x$'
res = (1440/80,900/80) # default dpi is 80

# calculate 3 to 1 ratio of upper to lower plot
left, width = 0.1, 0.8
c = max/4
rect1 = [left, c+0.1, width, c*3]
rect2 = [left, 0.1, width, c]

# title
title = "a = 1.15, w = 0.1, l = 3.0, eta1 = 1.0, eta2 = 1.5, theta = 0, alpha = 83"

# import data
print "reading bifurcation data..."
r = mlab.csv2rec(DATAX, delimiter='\t', comments='#')
print "done."
print "reading velocity data..."
s = mlab.csv2rec(DATAV, delimiter='\t', comments='#')
print "done."


xmin = r.r.min()
xmax = r.r.max()
vmin = s.x.min()
vmax = s.x.max()

xlim1 = xmin-xpad,xmax+xpad
xlim2 = xmin-xpad,xmax+xpad

ylim2 = vmin-ypad,vmax+ypad

# draw figure
fig = plt.figure(figsize=res)
#fig.suptitle(title, size=tsize)

# draw first subplot and setup axis
ax1 = fig.add_axes(rect1)
ax2 = fig.add_axes(rect2)

# put x into the plot
print "plotting bifurcation..."
ax1.plot(r.r, r.x, c='r', marker=',', ms=0.1, ls='')
ax1.set_xlim(xlim1)
ax1.set_ylim(ylim1)
ax1.set_yticks(ytks1)
ax1.set_ylabel(ylbl1, size=tsize)
ax1.set_xticklabels(xtkl1, size=ticksize)
ax1.set_yticklabels(ytkl1, size=ticksize)
ax1.axvline(1.82, color='black', ls=':', lw=4)
print "done."

# put <v> into the plot
print "plotting velocity..."
ax2.plot(s.r, s.x, c='b', marker=',', ms=0.1, ls='')
if has_temp is True:
	u = []
	for i in range(len(DATAEU)):
		u.append(mlab.csv2rec(DATAEU[i], delimiter='\t', comments='#'))
		ax2.plot(u[i].r, u[i].x, marker='.')
ax2.set_xlim(xlim2)
ax2.set_ylim(ylim2)
#ax2.set_xticks(xtks2)
#ax2.set_yticks(ytks2)
ax2.set_ylabel(ylbl2, size=tsize)
ax2.set_xlabel(xlbl2, size=tsize)
ax2.set_xticklabels(ax2.get_xticks(), size=ticksize)
ax2.set_yticklabels(ax2.get_yticks(), size=ticksize)
ax2.axvline(1.82, color='black', ls=':', lw=4)
print "done."

# adjust subplots
plt.subplots_adjust(hspace=0)

# stream the whole mess into a file
print "flushing into file..."
plt.savefig(FOUT)
print "done."

# EOf
